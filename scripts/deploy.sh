#REGISTRY_LOGIN=$1
#REGISTRY_PASSWORD=$2
#CI_PIPELINE_ID=$3

echo "\> mkdir -p /home/$DEPLOY_HOST_USER/docker/dailyplan_service/logs "
mkdir -p /home/$DEPLOY_HOST_USER/docker/dailyplan_service/logs

echo "\> mkdir -p /home/$DEPLOY_HOST_USER/docker/epictask_service/logs "
mkdir -p /home/$DEPLOY_HOST_USER/docker/epictask_service/logs

echo "\> mkdir -p /home/$DEPLOY_HOST_USER/docker/job_service/logs "
mkdir -p /home/$DEPLOY_HOST_USER/docker/job_service/logs

echo "\> mkdir -p /home/$DEPLOY_HOST_USER/docker/user_service/logs "
mkdir -p /home/$DEPLOY_HOST_USER/docker/user_service/logs

echo "\> mkdir -p /home/$DEPLOY_HOST_USER/docker/reverse_proxy/logs "
mkdir -p /home/$DEPLOY_HOST_USER/docker/reverse_proxy/logs

echo "\> docker stack ls "
docker stack  ls  

echo "\> docker node ls "
docker node   ls 

echo "\> docker login -u "\$REGISTRY_LOGIN" -p "\$REGISTRY_PASSWORD" "
docker login -u $REGISTRY_LOGIN -p $REGISTRY_PASSWORD

echo "\> env CI_PIPELINE_ID=$CI_PIPELINE_ID DEPLOY_HOST_USER=$DEPLOY_HOST_USER docker stack deploy  -c ./docker_stack.yml  stack  --with-registry-auth "
env CI_PIPELINE_ID=$CI_PIPELINE_ID DEPLOY_HOST_USER=$DEPLOY_HOST_USER \
docker stack deploy -c docker_stack.yml  stack  --with-registry-auth 
