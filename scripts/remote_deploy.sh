
# -------------------------------------- Sending docker_stack.yml --------------------------------------------------

echo "[INFO] Sending docker_stack.yml to $DEPLOY_HOST_USER@$DEPLOY_HOST_ADDRESS:~..."
echo "\> sshpass -p \$DEPLOY_HOST_USER_PASSWORD\ "
echo "   scp -o StrictHostKeyChecking=no docker_stack.yml $DEPLOY_HOST_USER@$DEPLOY_HOST_ADDRESS:~"

if sshpass -p $DEPLOY_HOST_USER_PASSWORD\
   scp -o StrictHostKeyChecking=no docker_stack.yml $DEPLOY_HOST_USER@$DEPLOY_HOST_ADDRESS:~;
then
	echo "[INFO] docker_stack.yml has been sent."
else
	echo "[ERROR] Couldnt send docker_stack.yml."
	exit 1
fi


# ------------------------------------------------------------------------------------------------------------------

# --------------------------------- Connecting to remote server ----------------------------------------------------

echo "[INFO] Connecting to $DEPLOY_HOST_ADDRESS..."
echo "\> sshpass -p \$DEPLOY_HOST_USER_PASSWORD\ "
echo "   ssh -o StrictHostKeyChecking=no $DEPLOY_HOST_USER@$DEPLOY_HOST_ADDRESS bash -s < scripts/deploy.sh\ "
echo " 																					 $REGISTRY_LOGIN \$REGISTRY_PASSWORD\ "
echo "                                                                                   $CI_PIPELINE_ID; "

if sshpass -p $DEPLOY_HOST_USER_PASSWORD\
   ssh -o StrictHostKeyChecking=no $DEPLOY_HOST_USER@$DEPLOY_HOST_ADDRESS bash -s < scripts/deploy.sh\
																					$REGISTRY_LOGIN $REGISTRY_PASSWORD\
																					$CI_PIPELINE_ID;
then
	echo "[INFO] Connection to $DEPLOY_HOST_ADDRES was closed."
	echo OK
else
	echo "[ERROR] Couldnt connect to $DEPLOY_HOST_ADDRESS"
	exit 2
fi



# ------------------------------------------------------------------------------------------------------------------

