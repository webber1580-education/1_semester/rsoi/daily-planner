﻿using HttpRequest;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserDatabase.Models;
using UserService.Exceptions;
using UserHttpService;
using EpicTaskService;
using DailyPlanService;
using JobService;
using JobService.Exceptions;
using DailyPlanService.Exceptions;
using EpicTaskHttpService.Exceptions;
using DailyPlanHttpService.Exceptions;
using JobHttpService.Exceptions;
using Common.ExceptionHandler;
using UserHttpService.Exceptions;
using EpicTaskService.Exceptions;

namespace UserService
{
    public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly IUserHttpService _userHttpService;
        private readonly IEpicTaskService _epicTaskService;
        private readonly IDailyPlanService _dailyPlanService;
        private readonly IJobService _jobService;

        public UserService(ILogger<UserService> logger, 
            IUserHttpService userHttpService,
            IEpicTaskService epicTaskService,
            IDailyPlanService dailyPlanService,
            IJobService jobService)
        {
            _logger = logger;
            _userHttpService = userHttpService;
            _epicTaskService = epicTaskService;
            _dailyPlanService = dailyPlanService;
            _jobService = jobService;
        }

        public async Task<int?> CreateUserAsync(User user)
        {
            int? result = null;

            try
            {
                result = await _userHttpService.CreateUserAsync(user);
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"UserService User creation failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new UserServiceException(userHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<User> GetUserAsync(int userId)
        {
            User result = null;

            try
            {
                result = await _userHttpService.GetUserAsync(userId);
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"UserService getting list of User failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new UserServiceException(userHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<List<User>> GetUsersListAsync()
        {
            List<User> result = null;

            try
            {
                result = await _userHttpService.GetUsersListAsync();
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"UserService getting User failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new UserServiceException(userHttpServiceEx.Message);
            }

            return result;
        }

        public async Task DeleteUserAsync(int userId)
        {
            var rollbackOperations = new LinkedList<Func<Task>>();

            try
            {
                var user = await _userHttpService.GetUserAsync(userId);
                var epicTaskList = await _epicTaskService.GetEpicTasksListAsync();
                var dailyPlanList = await _dailyPlanService.GetDailyPlansListAsync();
                var jobList = await _jobService.GetJobsListAsync();

                foreach (var epicTask in epicTaskList.Where(e => e.UserId == userId))
                {
                    await _epicTaskService.DeleteEpicTaskAsync(epicTask.Id);
                    rollbackOperations.AddFirst(async () => { await _epicTaskService.CreateEpicTaskAsync(epicTask); });
                }

                foreach (var dailyPlan in dailyPlanList.Where(d => d.UserId == userId))
                {
                    await _dailyPlanService.DeleteDailyPlanAsync(dailyPlan.Id);
                    rollbackOperations.AddFirst(async () => { await _dailyPlanService.CreateDailyPlanAsync(dailyPlan); });
                }

                foreach (var job in jobList.Where(j => j.UserId == userId))
                {
                    await _jobService.DeleteJobAsync(job.Id);
                    rollbackOperations.AddFirst(async () => { await _jobService.CreateJobAsync(job); });
                }

                await _userHttpService.DeleteUserAsync(userId);
            }
            catch (EpicTaskServiceException epicTaskServiceEx)
            {
                _logger.LogError($"UserService deleting User failled because of EpicTaskService error: {epicTaskServiceEx.Message}");
                var userServiceEx = new UserServiceException(epicTaskServiceEx.Message);
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, userServiceEx);
            }
            catch (DailyPlanServiceException dailyPlanServiceEx)
            {
                _logger.LogError($"UserService deleting User failled because of DailyPlanService error: {dailyPlanServiceEx.Message}");
                var userServiceEx = new UserServiceException(dailyPlanServiceEx.Message);
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, userServiceEx);
            }
            catch (JobServiceException jobServiceEx)
            {
                _logger.LogError($"UserService deleting User failled because of JobService error: {jobServiceEx.Message}");
                var userServiceEx = new UserServiceException(jobServiceEx.Message);
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, userServiceEx);
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"UserService deleting User failled because of UserHttpService error: {userHttpServiceEx.Message}");
                var userServiceEx = new UserServiceException(userHttpServiceEx.Message);
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, userServiceEx);
            }
        }

        public async Task UpdateUserAsync(User user)
        {
            try
            {
                await _userHttpService.UpdateUserAsync(user);
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"UserService updating User failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new UserServiceException(userHttpServiceEx.Message);
            }
        }
    }
}
