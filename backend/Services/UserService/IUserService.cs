﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserDatabase.Models;

namespace UserService
{
    public interface IUserService
    {
        Task<int?> CreateUserAsync(User user);

        Task<User> GetUserAsync(int userId);

        Task<List<User>> GetUsersListAsync();

        Task DeleteUserAsync(int userId);

        Task UpdateUserAsync(User user);
    }
}
