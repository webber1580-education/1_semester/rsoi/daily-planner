﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace UserService
{
    public static class UserServiceProviderExtensions
    {
        public static void AddUserService(this IServiceCollection services)
        {
            services.AddSingleton<IUserService, UserService>();
        }
    }
}
