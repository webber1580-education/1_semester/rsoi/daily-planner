﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EpicTaskDatabase.Models;
using JobDatabase.Models;

namespace GatewayService
{
    public interface IGatewayService
    {
        Task CreateJobInEpicTaskAsync(Job job, int epicTaskId);

        Task DeleteJobFromEpicTaskAsync(int jobId, int epicTaskId);

        Task CreateJobInDailyPlanAsync(Job job, int dailyPlanId);

        Task DeleteJobFromDailyPlanAsync(int jobId, int dailyPlanId);
    }
}
