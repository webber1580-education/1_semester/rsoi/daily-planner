﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace GatewayService
{
    public static class GatewayServiceProviderExtensions
    {
        public static void AddGatewayService(this IServiceCollection services)
        {
            services.AddSingleton<IGatewayService, GatewayService>();
        }
    }
}
