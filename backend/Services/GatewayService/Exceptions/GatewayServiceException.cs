﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayService.Exceptions
{
    [Serializable]
    public class GatewayServiceException : Exception
    {
        public GatewayServiceException()
            : base()
        {
        }

        public GatewayServiceException(string message)
            : base(message)
        {
        }

        public GatewayServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
