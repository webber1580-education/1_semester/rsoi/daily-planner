﻿using DailyPlanService;
using EpicTaskDatabase.Models;
using EpicTaskService;
using EpicTaskService.Exceptions;
using JobDatabase.Models;
using JobService;
using JobService.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.ExceptionHandler;
using DailyPlanDatabase.Models;
using DailyPlanService.Exceptions;

namespace GatewayService
{
    public class GatewayService : IGatewayService
    {
        private readonly ILogger<GatewayService> _logger;
        private readonly IEpicTaskService _epicTaskService;
        private readonly IDailyPlanService _dailyPlanService;
        private readonly IJobService _jobService;

        public GatewayService(ILogger<GatewayService> logger,
            IEpicTaskService epicTaskService,
            IDailyPlanService dailyPlanService,
            IJobService jobService)
        {
            _logger = logger;
            _epicTaskService = epicTaskService;
            _dailyPlanService = dailyPlanService;
            _jobService = jobService;
        }

        public async Task CreateJobInEpicTaskAsync(Job job, int epicTaskId)
        {
            var rollbackOperations = new LinkedList<Func<Task>>();

            try
            {
                var jobId = await _jobService.CreateJobAsync(job);
                rollbackOperations.AddFirst(async () => { await _jobService.DeleteJobAsync(jobId.Value); });

                var jobToEpicTask = new JobToEpicTask() { JobId = jobId.Value, EpicTaskId = epicTaskId };
                var jobToEpicTaskId = await _epicTaskService.CreateJobToEpicTaskAsync(jobToEpicTask);
            }
            catch (JobServiceException jobServiceEx)
            {
                _logger.LogError($"GatewayService creation Job in EpicTask failed because of JobService error: {jobServiceEx.Message}");
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, jobServiceEx);
            }
            catch (EpicTaskServiceException epicTaskServiceEx)
            {
                _logger.LogError($"GatewayService creation Job in EpicTask failed because of EpicTaskService error: {epicTaskServiceEx.Message}");
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, epicTaskServiceEx);
            }
        }

        public async Task DeleteJobFromEpicTaskAsync(int jobId, int epicTaskId)
        {
            // var rollbackOperations = new LinkedList<Func<Task>>();

            try
            {
                // var job = await _jobService.GetJobAsync(jobId);
                //var jobToEpicTaskList = await _epicTaskService.GetJobToEpicTasksListAsync();
                //var jobToEpicTask = jobToEpicTaskList.Find(e => e.JobId == jobId && e.EpicTaskId == epicTaskId);

                await _jobService.DeleteJobAsync(jobId);
                // rollbackOperations.AddFirst(async () => { await _jobService.CreateJobAsync(job); });

                //await _epicTaskService.DeleteJobToEpicTaskAsync(jobToEpicTask.Id);
                //rollbackOperations.AddFirst(async () => { await _epicTaskService.CreateJobToEpicTaskAsync(jobToEpicTask); });
            }
            catch (JobServiceException jobServiceEx)
            {
                _logger.LogError($"GatewayService creation Job in EpicTask failed because of JobService error: {jobServiceEx.Message}");
                // await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, jobServiceEx);
            }
            //catch (EpicTaskServiceException epicTaskServiceEx)
            //{
            //    _logger.LogError($"GatewayService creation Job in EpicTask failed because of EpicTaskService error: {epicTaskServiceEx.Message}");
            //    await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, epicTaskServiceEx);
            //}
            //catch (Exception Ex)
            //{
            //    _logger.LogError($"No such JobToEpicTask to remove");
            //    await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, Ex);
            //}
        }

        public async Task CreateJobInDailyPlanAsync(Job job, int dailyPlanId)
        {
            var rollbackOperations = new LinkedList<Func<Task>>();

            try
            {
                var jobId = await _jobService.CreateJobAsync(job);
                rollbackOperations.AddFirst(async () => { await _jobService.DeleteJobAsync(jobId.Value); });

                var jobToDailyPlan = new JobToDailyPlan() { JobId = jobId.Value, DailyPlanId = dailyPlanId };
                var jobToDailyPlanId = await _dailyPlanService.CreateJobToDailyPlanAsync(jobToDailyPlan);
            }
            catch (JobServiceException jobServiceEx)
            {
                _logger.LogError($"GatewayService creation Job in DailyPlan failed because of JobService error: {jobServiceEx.Message}");
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, jobServiceEx);
            }
            catch (DailyPlanServiceException dailyPlanServiceEx)
            {
                _logger.LogError($"GatewayService creation Job in DailyPlan failed because of DailyPlanService error: {dailyPlanServiceEx.Message}");
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, dailyPlanServiceEx);
            }
        }

        public async Task DeleteJobFromDailyPlanAsync(int jobId, int dailyPlanId)
        {
            // var rollbackOperations = new LinkedList<Func<Task>>();

            try
            {
                // var job = await _jobService.GetJobAsync(jobId);
                //var jobToDailyPlanList = await _dailyPlanService.GetJobToDailyPlansListAsync();
                //var jobToDailyPlan = jobToDailyPlanList.Find(e => e.JobId == jobId && e.DailyPlanId == dailyPlanId);

                await _jobService.DeleteJobAsync(jobId);
                // rollbackOperations.AddFirst(async () => { await _jobService.CreateJobAsync(job); });

                //await _dailyPlanService.DeleteJobToDailyPlanAsync(jobToDailyPlan.Id);
                //rollbackOperations.AddFirst(async () => { await _dailyPlanService.CreateJobToDailyPlanAsync(jobToDailyPlan); });
            }
            catch (JobServiceException jobServiceEx)
            {
                _logger.LogError($"GatewayService creation Job in DailyPlan failed because of JobService error: {jobServiceEx.Message}");
                // await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, jobServiceEx);
            }
            //catch (DailyPlanServiceException dailyPlanServiceEx)
            //{
            //    _logger.LogError($"GatewayService creation Job in DailyPlan failed because of DailyPlanService error: {dailyPlanServiceEx.Message}");
            //    await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, dailyPlanServiceEx);
            //}
        }
    }
}
