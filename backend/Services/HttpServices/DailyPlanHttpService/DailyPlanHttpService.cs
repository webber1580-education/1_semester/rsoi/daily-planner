﻿using HttpRequest;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DailyPlanDatabase.Models;
using DailyPlanHttpService.Configuration;
using DailyPlanHttpService.Exceptions;

namespace DailyPlanHttpService
{
    public class DailyPlanHttpService : IDailyPlanHttpService
    {
        private readonly ILogger<DailyPlanHttpService> _logger;
        private readonly IDailyPlanHttpServiceConfiguration _configuration;
        private readonly Uri _dailyPlanBaseUri;

        public DailyPlanHttpService(ILogger<DailyPlanHttpService> logger, IDailyPlanHttpServiceConfiguration configuration)
        {
            logger.LogTrace("Constructing DailyPlan.");

            _logger = logger;
            _configuration = configuration;

            UriBuilder baseUriBuilder = new UriBuilder("http", _configuration.DailyPlanHost, _configuration.DailyPlanPort);

            _dailyPlanBaseUri = new Uri(baseUriBuilder.Uri.ToString());
        }

        private string CreateRequestUri(string path = "")
        {
            return new Uri(_dailyPlanBaseUri, $"/api/dailyPlan{path}").AbsoluteUri;
        }

        private string CreateRequestJobUri(string path = "")
        {
            return new Uri(_dailyPlanBaseUri, $"api/jobtodailyplan{path}").AbsoluteUri;
        }

        #region DailyPlan

        public async Task<int?> CreateDailyPlanAsync(DailyPlan dailyPlan)
        {
            int? result = null;
            var postJsonRequestBody = JsonConvert.SerializeObject(dailyPlan);

            HttpResponseMessage response = await PostRequest.ExecuteAsync(CreateRequestUri(), postJsonRequestBody);

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"DailyPlan was created.");
                result = JsonConvert.DeserializeObject<int?>(resultContent);
            }
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException($"DailyPlan creation failed.");
            }

            return result;
        }

        public async Task<DailyPlan> GetDailyPlanAsync(int dailyPlanId)
        {
            DailyPlan result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestUri($"/{dailyPlanId}"));

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"Info of dailyPlan with id '{dailyPlanId} ' was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<DailyPlan>(resultContent);
            }
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException($"Info of dailyPlan with id '{dailyPlanId}' acquisition failed.");
            }

            return result;
        }

        public async Task<List<DailyPlan>> GetDailyPlansListAsync()
        {
            List<DailyPlan> result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestUri());

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"List of dailyPlans was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<List<DailyPlan>>(resultContent);
            }
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException("List of dailyPlan acquisition failed.");
            }

            return result;
        }

        public async Task DeleteDailyPlanAsync(int dailyPlanId)
        {
            HttpResponseMessage response = await DeleteRequest.ExecuteAsync(CreateRequestUri($"/{dailyPlanId}"));

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"DailyPlan with id '{dailyPlanId}' was deleted.");
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException($"Stream with name '{dailyPlanId}' deletion failed.");
            }
        }

        public async Task UpdateDailyPlanAsync(DailyPlan dailyPlan)
        {
            var putJsonRequestBody = JsonConvert.SerializeObject(dailyPlan);

            HttpResponseMessage response = await PutRequest.ExecuteAsync(CreateRequestUri(), putJsonRequestBody);

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"DailyPlan with id {dailyPlan.Id} was updated.");
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException($"DailyPlan updation failed.");
            }
        }

        #endregion

        #region JobToDailyPlan

        public async Task<int?> CreateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan)
        {
            int? result = null;
            var postJsonRequestBody = JsonConvert.SerializeObject(jobToDailyPlan);

            HttpResponseMessage response = await PostRequest.ExecuteAsync(CreateRequestJobUri(), postJsonRequestBody);

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"JobToDailyPlan was created.");
                result = JsonConvert.DeserializeObject<int?>(resultContent);
            }
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException($"JobToDailyPlan creation failed.");
            }

            return result;
        }

        public async Task<JobToDailyPlan> GetJobToDailyPlanAsync(int jobToDailyPlanId)
        {
            JobToDailyPlan result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestJobUri($"/{jobToDailyPlanId}"));

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"Info of jobToDailyPlan with id '{jobToDailyPlanId}' was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<JobToDailyPlan>(resultContent);
            }
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException($"Info of jobToDailyPlan with id '{jobToDailyPlanId}' acquisition failed.");
            }

            return result;
        }

        public async Task<List<JobToDailyPlan>> GetJobToDailyPlansListAsync()
        {
            List<JobToDailyPlan> result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestJobUri());

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"List of jobToDailyPlans was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<List<JobToDailyPlan>>(resultContent);
            }
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException("List of jobToDailyPlans acquisition failed.");
            }

            return result;
        }

        public async Task DeleteJobToDailyPlanAsync(int jobToDailyPlanId)
        {
            HttpResponseMessage response = await DeleteRequest.ExecuteAsync(CreateRequestJobUri($"/{jobToDailyPlanId}"));

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"JobToDailyPlan with id '{jobToDailyPlanId}' was deleted.");
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException($"JobToDailyPlan with name '{jobToDailyPlanId}' deletion failed.");
            }
        }

        public async Task UpdateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan)
        {
            var putJsonRequestBody = JsonConvert.SerializeObject(jobToDailyPlan);

            HttpResponseMessage response = await PutRequest.ExecuteAsync(CreateRequestJobUri(), putJsonRequestBody);

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"JobToDailyPlan with id {jobToDailyPlan.Id} was updated.");
            else
            {
                _logger.LogError($"DailyPlanWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new DailyPlanHttpServiceException($"JobToDailyPlan updation failed.");
            }
        }

        #endregion

    }
}
