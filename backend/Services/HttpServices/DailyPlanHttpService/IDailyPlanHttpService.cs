﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DailyPlanDatabase.Models;

namespace DailyPlanHttpService
{
    public interface IDailyPlanHttpService
    {
        #region DailyPlan

        Task<int?> CreateDailyPlanAsync(DailyPlan dailyPlan);

        Task<DailyPlan> GetDailyPlanAsync(int dailyPlanId);

        Task<List<DailyPlan>> GetDailyPlansListAsync();

        Task DeleteDailyPlanAsync(int dailyPlanId);

        Task UpdateDailyPlanAsync(DailyPlan dailyPlan);

        #endregion

        #region JobToDailyPlan

        Task<int?> CreateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan);

        Task<JobToDailyPlan> GetJobToDailyPlanAsync(int jobToDailyPlanId);

        Task<List<JobToDailyPlan>> GetJobToDailyPlansListAsync();

        Task DeleteJobToDailyPlanAsync(int jobToDailyPlanId);

        Task UpdateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan);

        #endregion
    }
}
