﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using DailyPlanHttpService.Configuration;

namespace DailyPlanHttpService
{
    public static class DailyPlanHttpServiceProviderExtensions
    {
        public static void AddDailyPlanHttpService(this IServiceCollection services, IConfigurationSection configuration)
        {
            var config = configuration.Get<DailyPlanHttpServiceConfiguration>();
            services.AddTransient<IDailyPlanHttpServiceConfiguration>(serviceProvider => config);
            services.AddSingleton<IDailyPlanHttpService, DailyPlanHttpService>();
        }
    }
}
