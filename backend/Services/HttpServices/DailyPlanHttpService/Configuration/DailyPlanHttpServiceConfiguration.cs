﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DailyPlanHttpService.Configuration
{
    public class DailyPlanHttpServiceConfiguration : IDailyPlanHttpServiceConfiguration
    {
        public string DailyPlanHost { get; set; }

        public int DailyPlanPort { get; set; }
    }
}
