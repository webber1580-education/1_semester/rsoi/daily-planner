﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DailyPlanHttpService.Configuration
{
    public interface IDailyPlanHttpServiceConfiguration
    {
        string DailyPlanHost { get; set; }

        int DailyPlanPort { get; set; }
    }
}
