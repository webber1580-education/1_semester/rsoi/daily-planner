﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DailyPlanHttpService.Exceptions
{
    [Serializable]
    public class DailyPlanHttpServiceException : Exception
    {
        public DailyPlanHttpServiceException()
            : base()
        {
        }

        public DailyPlanHttpServiceException(string message)
            : base(message)
        {
        }

        public DailyPlanHttpServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
