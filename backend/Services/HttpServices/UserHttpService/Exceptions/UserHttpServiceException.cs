﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserHttpService.Exceptions
{
    [Serializable]
    public class UserHttpServiceException : Exception
    {
        public UserHttpServiceException()
            : base()
        {
        }

        public UserHttpServiceException(string message)
            : base(message)
        {
        }

        public UserHttpServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
