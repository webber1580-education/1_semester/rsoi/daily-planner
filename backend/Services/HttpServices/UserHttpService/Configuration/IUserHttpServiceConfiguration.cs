﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserHttpService.Configuration
{
    public interface IUserHttpServiceConfiguration
    {
        string UserHost { get; set; }

        int UserPort { get; set; }
    }
}
