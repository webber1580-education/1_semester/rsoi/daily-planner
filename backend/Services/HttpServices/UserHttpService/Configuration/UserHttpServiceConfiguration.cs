﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserHttpService.Configuration
{
    public class UserHttpServiceConfiguration : IUserHttpServiceConfiguration
    {
        public string UserHost { get; set; }

        public int UserPort { get; set; }
    }
}
