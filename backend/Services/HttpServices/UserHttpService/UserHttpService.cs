﻿using HttpRequest;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UserDatabase.Models;
using UserHttpService.Configuration;
using UserHttpService.Exceptions;

namespace UserHttpService
{
    public class UserHttpService : IUserHttpService
    {
        private readonly ILogger<UserHttpService> _logger;
        private readonly IUserHttpServiceConfiguration _configuration;
        private readonly Uri _userBaseUri;

        public UserHttpService(ILogger<UserHttpService> logger, IUserHttpServiceConfiguration configuration)
        {
            logger.LogTrace("Constructing User.");

            _logger = logger;
            _configuration = configuration;

            UriBuilder baseUriBuilder = new UriBuilder("http", _configuration.UserHost, _configuration.UserPort);

            _userBaseUri = new Uri(baseUriBuilder.Uri.ToString());
        }

        private string CreateRequestUri(string path = "")
        {
            return new Uri(_userBaseUri, $"/api/user{path}").AbsoluteUri;
        }

        public async Task<int?> CreateUserAsync(User user)
        {
            int? result = null;
            var postJsonRequestBody = JsonConvert.SerializeObject(user);

            HttpResponseMessage response = await PostRequest.ExecuteAsync(CreateRequestUri(), postJsonRequestBody);

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"User was created.");
                result = JsonConvert.DeserializeObject<int?>(resultContent);
            }
            else
            {
                _logger.LogError($"UserWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new UserHttpServiceException($"User creation failed.");
            }

            return result;
        }

        public async Task<User> GetUserAsync(int userId)
        {
            User result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestUri($"/{userId}"));

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"Info of user with id '{userId} ' was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<User>(resultContent);
            }
            else
            {
                _logger.LogError($"UserWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new UserHttpServiceException($"Info of user with id '{userId}' acquisition failed.");
            }

            return result;
        }

        public async Task<List<User>> GetUsersListAsync()
        {
            List<User> result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestUri());

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"List of users was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<List<User>>(resultContent);
            }
            else
            {
                _logger.LogError($"UserWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new UserHttpServiceException("List of user acquisition failed.");
            }

            return result;
        }

        public async Task DeleteUserAsync(int userId)
        {
            HttpResponseMessage response = await DeleteRequest.ExecuteAsync(CreateRequestUri($"/{userId}"));

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"User with id '{userId}' was deleted.");
            else
            {
                _logger.LogError($"UserWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new UserHttpServiceException($"User with name '{userId}' deletion failed.");
            }
        }

        public async Task UpdateUserAsync(User user)
        {
            var putJsonRequestBody = JsonConvert.SerializeObject(user);

            HttpResponseMessage response = await PutRequest.ExecuteAsync(CreateRequestUri(), putJsonRequestBody);

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"User with id {user.Id} was updated.");
            else
            {
                _logger.LogError($"UserWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new UserHttpServiceException($"User updation failed.");
            }
        }
    }
}
