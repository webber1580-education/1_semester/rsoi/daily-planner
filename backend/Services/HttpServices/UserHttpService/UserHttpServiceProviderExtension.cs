﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using UserHttpService.Configuration;

namespace UserHttpService
{
    public static class UserHttpServiceProviderExtensions
    {
        public static void AddUserHttpService(this IServiceCollection services, IConfigurationSection configuration)
        {
            var config = configuration.Get<UserHttpServiceConfiguration>();
            services.AddTransient<IUserHttpServiceConfiguration>(serviceProvider => config);
            services.AddSingleton<IUserHttpService, UserHttpService>();
        }
    }
}
