﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobHttpService.Configuration
{
    public class JobHttpServiceConfiguration : IJobHttpServiceConfiguration
    {
        public string JobHost { get; set; }

        public int JobPort { get; set; }
    }
}
