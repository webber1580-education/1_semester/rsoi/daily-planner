﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobHttpService.Configuration
{
    public interface IJobHttpServiceConfiguration
    {
        string JobHost { get; set; }

        int JobPort { get; set; }
    }
}
