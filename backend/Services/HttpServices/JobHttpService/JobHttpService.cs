﻿using HttpRequest;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using JobDatabase.Models;
using JobHttpService.Configuration;
using JobHttpService.Exceptions;

namespace JobHttpService
{
    public class JobHttpService : IJobHttpService
    {
        private readonly ILogger<JobHttpService> _logger;
        private readonly IJobHttpServiceConfiguration _configuration;
        private readonly Uri _jobBaseUri;

        public JobHttpService(ILogger<JobHttpService> logger, IJobHttpServiceConfiguration configuration)
        {
            logger.LogTrace("Constructing Job.");

            _logger = logger;
            _configuration = configuration;

            UriBuilder baseUriBuilder = new UriBuilder("http", _configuration.JobHost, _configuration.JobPort);

            _jobBaseUri = new Uri(baseUriBuilder.Uri.ToString());
        }

        private string CreateRequestUri(string path = "")
        {
            return new Uri(_jobBaseUri, $"/api/job{path}").AbsoluteUri;
        }

        public async Task<int?> CreateJobAsync(Job job)
        {
            int? result = null;
            var postJsonRequestBody = JsonConvert.SerializeObject(job);

            HttpResponseMessage response = await PostRequest.ExecuteAsync(CreateRequestUri(), postJsonRequestBody);

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"Job was created.");
                result = JsonConvert.DeserializeObject<int?>(resultContent);
            }
            else
            {
                _logger.LogError($"JobWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new JobHttpServiceException($"Job creation failed.");
            }

            return result;
        }

        public async Task<Job> GetJobAsync(int jobId)
        {
            Job result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestUri($"/{jobId}"));

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"Info of job with id '{jobId} ' was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<Job>(resultContent);
            }
            else
            {
                _logger.LogError($"JobWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new JobHttpServiceException($"Info of job with id '{jobId}' acquisition failed.");
            }

            return result;
        }

        public async Task<List<Job>> GetJobsListAsync()
        {
            List<Job> result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestUri());

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"List of jobs was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<List<Job>>(resultContent);
            }
            else
            {
                _logger.LogError($"JobWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new JobHttpServiceException("List of job acquisition failed.");
            }

            return result;
        }

        public async Task DeleteJobAsync(int jobId)
        {
            HttpResponseMessage response = await DeleteRequest.ExecuteAsync(CreateRequestUri($"/{jobId}"));

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"Job with id '{jobId}' was deleted.");
            else
            {
                _logger.LogError($"JobWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new JobHttpServiceException($"Job with name '{jobId}' deletion failed.");
            }
        }

        public async Task UpdateJobAsync(Job job)
        {
            var putJsonRequestBody = JsonConvert.SerializeObject(job);

            HttpResponseMessage response = await PutRequest.ExecuteAsync(CreateRequestUri(), putJsonRequestBody);

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"Job with id {job.Id} was updated.");
            else
            {
                _logger.LogError($"JobWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new JobHttpServiceException($"Job updation failed.");
            }
        }
    }
}
