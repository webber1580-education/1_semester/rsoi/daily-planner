﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JobDatabase.Models;

namespace JobHttpService
{
    public interface IJobHttpService
    {
        Task<int?> CreateJobAsync(Job job);

        Task<Job> GetJobAsync(int jobId);

        Task<List<Job>> GetJobsListAsync();

        Task DeleteJobAsync(int jobId);

        Task UpdateJobAsync(Job job);
    }
}
