﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using JobHttpService.Configuration;

namespace JobHttpService
{
    public static class JobHttpServiceProviderExtensions
    {
        public static void AddJobHttpService(this IServiceCollection services, IConfigurationSection configuration)
        {
            var config = configuration.Get<JobHttpServiceConfiguration>();
            services.AddTransient<IJobHttpServiceConfiguration>(serviceProvider => config);
            services.AddSingleton<IJobHttpService, JobHttpService>();
        }
    }
}
