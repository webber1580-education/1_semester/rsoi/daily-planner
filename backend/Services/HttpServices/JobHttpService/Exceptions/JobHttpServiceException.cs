﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobHttpService.Exceptions
{
    [Serializable]
    public class JobHttpServiceException : Exception
    {
        public JobHttpServiceException()
            : base()
        {
        }

        public JobHttpServiceException(string message)
            : base(message)
        {
        }

        public JobHttpServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
