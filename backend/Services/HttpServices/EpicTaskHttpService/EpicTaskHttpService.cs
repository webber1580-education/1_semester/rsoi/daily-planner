﻿using HttpRequest;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EpicTaskDatabase.Models;
using EpicTaskHttpService.Configuration;
using EpicTaskHttpService.Exceptions;

namespace EpicTaskHttpService
{
    public class EpicTaskHttpService : IEpicTaskHttpService
    {
        private readonly ILogger<EpicTaskHttpService> _logger;
        private readonly IEpicTaskHttpServiceConfiguration _configuration;
        private readonly Uri _epicTaskBaseUri;

        public EpicTaskHttpService(ILogger<EpicTaskHttpService> logger, IEpicTaskHttpServiceConfiguration configuration)
        {
            logger.LogTrace("Constructing EpicTask.");

            _logger = logger;
            _configuration = configuration;

            UriBuilder baseUriBuilder = new UriBuilder("http", _configuration.EpicTaskHost, _configuration.EpicTaskPort);

            _epicTaskBaseUri = new Uri(baseUriBuilder.Uri.ToString());
        }

        private string CreateRequestUri(string path = "")
        {
            return new Uri(_epicTaskBaseUri, $"/api/epicTask{path}").AbsoluteUri;
        }

        private string CreateRequestJobUri(string path = "")
        {
            return new Uri(_epicTaskBaseUri, $"api/jobtoepicTask{path}").AbsoluteUri;
        }

        #region EpicTask

        public async Task<int?> CreateEpicTaskAsync(EpicTask epicTask)
        {
            int? result = null;
            var postJsonRequestBody = JsonConvert.SerializeObject(epicTask);

            HttpResponseMessage response = await PostRequest.ExecuteAsync(CreateRequestUri(), postJsonRequestBody);

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"EpicTask was created.");
                result = JsonConvert.DeserializeObject<int?>(resultContent);
            }
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException($"EpicTask creation failed.");
            }

            return result;
        }

        public async Task<EpicTask> GetEpicTaskAsync(int epicTaskId)
        {
            EpicTask result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestUri($"/{epicTaskId}"));

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"Info of epicTask with id '{epicTaskId} ' was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<EpicTask>(resultContent);
            }
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException($"Info of epicTask with id '{epicTaskId}' acquisition failed.");
            }

            return result;
        }

        public async Task<List<EpicTask>> GetEpicTasksListAsync()
        {
            List<EpicTask> result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestUri());

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"List of epicTasks was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<List<EpicTask>>(resultContent);
            }
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException("List of epicTask acquisition failed.");
            }

            return result;
        }

        public async Task DeleteEpicTaskAsync(int epicTaskId)
        {
            HttpResponseMessage response = await DeleteRequest.ExecuteAsync(CreateRequestUri($"/{epicTaskId}"));

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"EpicTask with id '{epicTaskId}' was deleted.");
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException($"Stream with name '{epicTaskId}' deletion failed.");
            }
        }

        public async Task UpdateEpicTaskAsync(EpicTask epicTask)
        {
            var putJsonRequestBody = JsonConvert.SerializeObject(epicTask);

            HttpResponseMessage response = await PutRequest.ExecuteAsync(CreateRequestUri(), putJsonRequestBody);

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"EpicTask with id {epicTask.Id} was updated.");
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException($"EpicTask updation failed.");
            }
        }

        #endregion

        #region JobToEpicTask

        public async Task<int?> CreateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask)
        {
            int? result = null;
            var postJsonRequestBody = JsonConvert.SerializeObject(jobToEpicTask);

            HttpResponseMessage response = await PostRequest.ExecuteAsync(CreateRequestJobUri(), postJsonRequestBody);

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"JobToEpicTask was created.");
                result = JsonConvert.DeserializeObject<int?>(resultContent);
            }
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException($"JobToEpicTask creation failed.");
            }

            return result;
        }

        public async Task<JobToEpicTask> GetJobToEpicTaskAsync(int jobToEpicTaskId)
        {
            JobToEpicTask result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestJobUri($"/{jobToEpicTaskId}"));

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"Info of jobToEpicTask with id '{jobToEpicTaskId}' was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<JobToEpicTask>(resultContent);
            }
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException($"Info of jobToEpicTask with id '{jobToEpicTaskId}' acquisition failed.");
            }

            return result;
        }

        public async Task<List<JobToEpicTask>> GetJobToEpicTasksListAsync()
        {
            List<JobToEpicTask> result = null;

            HttpResponseMessage response = await GetRequest.ExecuteAsync(CreateRequestJobUri());

            if (response.IsSuccessStatusCode)
            {
                var resultContent = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"List of jobToEpicTasks was acquired: {resultContent}");
                result = JsonConvert.DeserializeObject<List<JobToEpicTask>>(resultContent);
            }
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException("List of jobToEpicTasks acquisition failed.");
            }

            return result;
        }

        public async Task DeleteJobToEpicTaskAsync(int jobToEpicTaskId)
        {
            HttpResponseMessage response = await DeleteRequest.ExecuteAsync(CreateRequestJobUri($"/{jobToEpicTaskId}"));

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"JobToEpicTask with id '{jobToEpicTaskId}' was deleted.");
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException($"JobToEpicTask with name '{jobToEpicTaskId}' deletion failed.");
            }
        }

        public async Task UpdateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask)
        {
            var putJsonRequestBody = JsonConvert.SerializeObject(jobToEpicTask);

            HttpResponseMessage response = await PutRequest.ExecuteAsync(CreateRequestJobUri(), putJsonRequestBody);

            if (response.IsSuccessStatusCode)
                _logger.LogInformation($"JobToEpicTask with id {jobToEpicTask.Id} was updated.");
            else
            {
                _logger.LogError($"EpicTaskWebHttpService Api call failed with statuscode {response.StatusCode}");
                throw new EpicTaskHttpServiceException($"JobToEpicTask updation failed.");
            }
        }

        #endregion

    }
}
