﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EpicTaskDatabase.Models;

namespace EpicTaskHttpService
{
    public interface IEpicTaskHttpService
    {
        #region EpicTask

        Task<int?> CreateEpicTaskAsync(EpicTask epicTask);

        Task<EpicTask> GetEpicTaskAsync(int epicTaskId);

        Task<List<EpicTask>> GetEpicTasksListAsync();

        Task DeleteEpicTaskAsync(int epicTaskId);

        Task UpdateEpicTaskAsync(EpicTask epicTask);

        #endregion

        #region JobToEpicTask

        Task<int?> CreateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask);

        Task<JobToEpicTask> GetJobToEpicTaskAsync(int jobToEpicTaskId);

        Task<List<JobToEpicTask>> GetJobToEpicTasksListAsync();

        Task DeleteJobToEpicTaskAsync(int jobToEpicTaskId);

        Task UpdateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask);

        #endregion
    }
}
