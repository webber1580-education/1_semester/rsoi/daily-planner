﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpicTaskHttpService.Exceptions
{
    [Serializable]
    public class EpicTaskHttpServiceException : Exception
    {
        public EpicTaskHttpServiceException()
            : base()
        {
        }

        public EpicTaskHttpServiceException(string message)
            : base(message)
        {
        }

        public EpicTaskHttpServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
