﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using EpicTaskHttpService.Configuration;

namespace EpicTaskHttpService
{
    public static class EpicTaskHttpServiceProviderExtensions
    {
        public static void AddEpicTaskHttpService(this IServiceCollection services, IConfigurationSection configuration)
        {
            var config = configuration.Get<EpicTaskHttpServiceConfiguration>();
            services.AddTransient<IEpicTaskHttpServiceConfiguration>(serviceProvider => config);
            services.AddSingleton<IEpicTaskHttpService, EpicTaskHttpService>();
        }
    }
}
