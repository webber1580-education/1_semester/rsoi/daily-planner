﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpicTaskHttpService.Configuration
{
    public class EpicTaskHttpServiceConfiguration : IEpicTaskHttpServiceConfiguration
    {
        public string EpicTaskHost { get; set; }

        public int EpicTaskPort { get; set; }
    }
}
