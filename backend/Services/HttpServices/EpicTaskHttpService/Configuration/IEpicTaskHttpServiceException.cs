﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpicTaskHttpService.Configuration
{
    public interface IEpicTaskHttpServiceConfiguration
    {
        string EpicTaskHost { get; set; }

        int EpicTaskPort { get; set; }
    }
}
