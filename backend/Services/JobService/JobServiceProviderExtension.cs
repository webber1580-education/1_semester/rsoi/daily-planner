﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace JobService
{
    public static class JobServiceProviderExtensions
    {
        public static void AddJobService(this IServiceCollection services)
        {
            services.AddSingleton<IJobService, JobService>();
        }
    }
}
