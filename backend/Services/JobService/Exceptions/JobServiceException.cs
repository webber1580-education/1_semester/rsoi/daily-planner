﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobService.Exceptions
{
    [Serializable]
    public class JobServiceException : Exception
    {
        public JobServiceException()
            : base()
        {
        }

        public JobServiceException(string message)
            : base(message)
        {
        }

        public JobServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
