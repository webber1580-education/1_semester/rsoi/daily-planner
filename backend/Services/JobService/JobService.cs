﻿using HttpRequest;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using JobDatabase.Models;
using JobService.Exceptions;
using JobHttpService;
using System.Linq;
using JobHttpService.Exceptions;
using UserHttpService;
using UserHttpService.Exceptions;
using Common.ExceptionHandler;
using DailyPlanHttpService;
using EpicTaskHttpService;
using EpicTaskHttpService.Exceptions;
using DailyPlanHttpService.Exceptions;

namespace JobService
{
    public class JobService : IJobService
    {
        private readonly ILogger<JobService> _logger;
        private readonly IJobHttpService _jobHttpService;
        private readonly IUserHttpService _userHttpService;
        private readonly IDailyPlanHttpService _dailyPlanHttpService;
        private readonly IEpicTaskHttpService _epicTaskHttpService;

        public JobService(ILogger<JobService> logger,
            IJobHttpService jobHttpService,
            IUserHttpService userHttpService,
            IDailyPlanHttpService dailyPlanHttpService,
            IEpicTaskHttpService epicTaskHttpService)
        {
            _logger = logger;
            _jobHttpService = jobHttpService;
            _userHttpService = userHttpService;
            _dailyPlanHttpService = dailyPlanHttpService;
            _epicTaskHttpService = epicTaskHttpService;
        }

        public async Task<int?> CreateJobAsync(Job job)
        {
            int? result = null;

            try
            {
                var userList = await _userHttpService.GetUsersListAsync();

                if (userList.FirstOrDefault(u => u.Id == job.UserId) != null)
                    result = await _jobHttpService.CreateJobAsync(job);
                else
                {
                    _logger.LogError("JobService creating Job failed because of userId");
                    throw new JobServiceException("No such userId");
                }
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"JobService creating Job failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new JobServiceException(userHttpServiceEx.Message);
            }
            catch (JobHttpServiceException jobHttpServiceEx)
            {
                _logger.LogError($"JobService creating Job failled because of " +
                    $"JobHttpService error: {jobHttpServiceEx.Message}");
                throw new JobServiceException(jobHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<Job> GetJobAsync(int jobId)
        {
            Job result = null;

            try
            {
                result = await _jobHttpService.GetJobAsync(jobId);
            }
            catch (JobHttpServiceException jobHttpServiceEx)
            {
                _logger.LogError($"JobService getting Job failled because of " +
                    $"JobHttpService error: {jobHttpServiceEx.Message}");
                throw new JobServiceException(jobHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<List<Job>> GetJobsListAsync()
        {
            List<Job> result = null;

            try
            {
                result = await _jobHttpService.GetJobsListAsync();
            }
            catch (JobHttpServiceException jobHttpServiceEx)
            {
                _logger.LogError($"JobService getting list of Job failled because of " +
                    $"JobHttpService error: {jobHttpServiceEx.Message}");
                throw new JobServiceException(jobHttpServiceEx.Message);
            }

            return result;
        }

        public async Task DeleteJobAsync(int jobId)
        {
            var rollbackOperations = new LinkedList<Func<Task>>();

            try
            {
                var jobToEpicTaskList = await _epicTaskHttpService.GetJobToEpicTasksListAsync();
                var jobToDailyPlanList = await _dailyPlanHttpService.GetJobToDailyPlansListAsync();

                foreach (var jobToEpicTask in jobToEpicTaskList.Where(j => j.JobId == jobId))
                {
                    await _epicTaskHttpService.DeleteJobToEpicTaskAsync(jobToEpicTask.Id);
                    rollbackOperations.AddFirst(async () => { await _epicTaskHttpService.CreateJobToEpicTaskAsync(jobToEpicTask); });
                }

                foreach (var jobToDailyPlan in jobToDailyPlanList.Where(j => j.JobId == jobId))
                {
                    await _dailyPlanHttpService.DeleteJobToDailyPlanAsync(jobToDailyPlan.Id);
                    rollbackOperations.AddFirst(async () => { await _dailyPlanHttpService.CreateJobToDailyPlanAsync(jobToDailyPlan); });
                }
                await _jobHttpService.DeleteJobAsync(jobId);
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {

                _logger.LogError($"JobService deleting Job failled because of " +
                    $"EpicTaskHttpServiceException error: {epicTaskHttpServiceEx.Message}");
                var jobServiceEx = new JobServiceException(epicTaskHttpServiceEx.Message);
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, jobServiceEx);
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {

                _logger.LogError($"JobService deleting Job failled because of " +
                    $"DailyPlanHttpServiceException error: {dailyPlanHttpServiceEx.Message}");
                var jobServiceEx = new JobServiceException(dailyPlanHttpServiceEx.Message);
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, jobServiceEx);
            }
            catch (JobHttpServiceException jobHttpServiceEx)
            {
                _logger.LogError($"JobService deleting Job failled because of " +
                    $"JobHttpService error: {jobHttpServiceEx.Message}");
                throw new JobServiceException(jobHttpServiceEx.Message);
            }
        }

        public async Task UpdateJobAsync(Job job)
        {
            try
            {
                var userList = await _userHttpService.GetUsersListAsync();

                if (userList.FirstOrDefault(u => u.Id == job.UserId) != null)
                    await _jobHttpService.UpdateJobAsync(job);
                else
                {
                    _logger.LogError("JobService updating Job failed because of userId");
                    throw new JobServiceException("No such userId");
                }
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"JobService updating Job failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new JobServiceException(userHttpServiceEx.Message);
            }
            catch (JobHttpServiceException jobHttpServiceEx)
            {
                _logger.LogError($"JobService updating Job failled because of " +
                    $"JobHttpService error: {jobHttpServiceEx.Message}");
                throw new JobServiceException(jobHttpServiceEx.Message);
            }
        }
    }
}
