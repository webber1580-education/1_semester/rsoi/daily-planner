﻿using HttpRequest;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EpicTaskDatabase.Models;
using EpicTaskService.Exceptions;
using EpicTaskHttpService;
using JobHttpService;
using System.Linq;
using EpicTaskHttpService.Exceptions;
using JobHttpService.Exceptions;
using UserHttpService;
using UserHttpService.Exceptions;
using Common.ExceptionHandler;

namespace EpicTaskService
{
    public class EpicTaskService : IEpicTaskService
    {
        private readonly ILogger<EpicTaskService> _logger;
        private readonly IEpicTaskHttpService _epicTaskHttpService;
        private readonly IJobHttpService _jobHttpService;
        private readonly IUserHttpService _userHttpService;

        public EpicTaskService(ILogger<EpicTaskService> logger,
            IEpicTaskHttpService epicTaskHttpService,
            IJobHttpService jobHttpService,
            IUserHttpService userHttpService)
        {
            _logger = logger;
            _epicTaskHttpService = epicTaskHttpService;
            _jobHttpService = jobHttpService;
            _userHttpService = userHttpService;
        }

        #region EpicTask

        public async Task<int?> CreateEpicTaskAsync(EpicTask epicTask)
        {
            int? result = null;

            try
            {
                var userList = await _userHttpService.GetUsersListAsync();
                var epicTaskList = await _epicTaskHttpService.GetEpicTasksListAsync();

                if (userList.FirstOrDefault(u => u.Id == epicTask.UserId) != null)
                    result = await _epicTaskHttpService.CreateEpicTaskAsync(epicTask);
                else
                {
                    _logger.LogError("EpicTaskService creating EpicTask failed because of userId");
                    throw new EpicTaskServiceException("No such userId");
                }
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService creating EpicTask failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new EpicTaskServiceException(userHttpServiceEx.Message);
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService creating EpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                throw new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<EpicTask> GetEpicTaskAsync(int epicTaskId)
        {
            EpicTask result = null;

            try
            {
                result = await _epicTaskHttpService.GetEpicTaskAsync(epicTaskId);
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService getting EpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                throw new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<List<EpicTask>> GetEpicTasksListAsync()
        {
            List<EpicTask> result = null;

            try
            {
                result = await _epicTaskHttpService.GetEpicTasksListAsync();
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService getting list of EpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                throw new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
            }

            return result;
        }

        public async Task DeleteEpicTaskAsync(int epicTaskId)
        {
            var rollbackOperations = new LinkedList<Func<Task>>();

            try
            {
                var jobToEpicTaskList = await _epicTaskHttpService.GetJobToEpicTasksListAsync();

                foreach (var jobToEpicTask in jobToEpicTaskList.Where(j => j.EpicTaskId == epicTaskId))
                {
                    await _epicTaskHttpService.DeleteJobToEpicTaskAsync(jobToEpicTask.Id);
                    rollbackOperations.AddFirst(async () => { await _epicTaskHttpService.CreateJobToEpicTaskAsync(jobToEpicTask); });
                }
                await _epicTaskHttpService.DeleteEpicTaskAsync(epicTaskId);
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {

                _logger.LogError($"EpicTaskService deleting EpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                var epicTaskServiceEx = new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, epicTaskServiceEx);
            }
        }

        public async Task UpdateEpicTaskAsync(EpicTask epicTask)
        {
            try
            {
                var userList = await _userHttpService.GetUsersListAsync();

                if (userList.FirstOrDefault(u => u.Id == epicTask.UserId) != null)
                    await _epicTaskHttpService.UpdateEpicTaskAsync(epicTask);
                else
                {
                    _logger.LogError("EpicTaskService updating EpicTask failed because of userId");
                    throw new EpicTaskServiceException("No such userId");
                }
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService updating EpicTask failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new EpicTaskServiceException(userHttpServiceEx.Message);
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService updating EpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                throw new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
            }
        }

        #endregion

        #region JobToEpicTask

        public async Task<int?> CreateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask)
        {
            int? result = null;

            try
            {
                var jobToEpicTaskList = await _epicTaskHttpService.GetJobToEpicTasksListAsync();
                var epicTaskList = await _epicTaskHttpService.GetEpicTasksListAsync();
                var jobList = await _jobHttpService.GetJobsListAsync();

                if (jobToEpicTaskList.FirstOrDefault(j => j.JobId == jobToEpicTask.JobId
                        && j.EpicTaskId == jobToEpicTask.EpicTaskId) != null)
                {
                    _logger.LogError("EpicTaskService creating JobToEpicTask failed because it's already exists");
                    throw new EpicTaskServiceException("JobToEpicTask is already exists");
                }
                else if (epicTaskList.FirstOrDefault(d => d.Id == jobToEpicTask.EpicTaskId) != null
                            && jobList.FirstOrDefault(j => j.Id == jobToEpicTask.JobId) != null)
                    result = await _epicTaskHttpService.CreateJobToEpicTaskAsync(jobToEpicTask);
                else
                {
                    _logger.LogError("EpicTaskService creating JobToEpicTask failed because of invalid jobId or epicTaskId");
                    throw new EpicTaskServiceException("No such jobId or epicTaskId");
                }
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService creating JobToEpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                throw new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
            }
            catch (JobHttpServiceException jobHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService creating JobToEpicTask failled because of " +
                    $"JobHttpService error: {jobHttpServiceEx.Message}");
                throw new EpicTaskServiceException(jobHttpServiceEx.Message);

            }

            return result;
        }

        public async Task<JobToEpicTask> GetJobToEpicTaskAsync(int jobToEpicTaskId)
        {
            JobToEpicTask result = null;

            try
            {
                result = await _epicTaskHttpService.GetJobToEpicTaskAsync(jobToEpicTaskId);
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService getting JobToEpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                throw new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<List<JobToEpicTask>> GetJobToEpicTasksListAsync()
        {
            List<JobToEpicTask> result = null;

            try
            {
                result = await _epicTaskHttpService.GetJobToEpicTasksListAsync();
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService getting list of JobToEpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                throw new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
            }

            return result;
        }

        public async Task DeleteJobToEpicTaskAsync(int jobToEpicTaskId)
        {
            try
            {
                await _epicTaskHttpService.DeleteJobToEpicTaskAsync(jobToEpicTaskId);
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService deleting JobToEpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                throw new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
            }
        }

        public async Task UpdateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask)
        {
            try
            {
                var jobToEpicTaskList = await _epicTaskHttpService.GetJobToEpicTasksListAsync();
                var epicTaskList = await _epicTaskHttpService.GetEpicTasksListAsync();
                var jobList = await _jobHttpService.GetJobsListAsync();

                if (jobToEpicTaskList.FirstOrDefault(j => j.JobId == jobToEpicTask.JobId
                        && j.EpicTaskId == jobToEpicTask.EpicTaskId) != null)
                {
                    _logger.LogError("EpicTaskService updating JobToEpicTask failed because it's already exists");
                    throw new EpicTaskServiceException("JobToEpicTask is already exists");
                }
                else if (epicTaskList.FirstOrDefault(d => d.Id == jobToEpicTask.EpicTaskId) != null
                            && jobList.FirstOrDefault(j => j.Id == jobToEpicTask.JobId) != null)
                    await _epicTaskHttpService.UpdateJobToEpicTaskAsync(jobToEpicTask);
                else
                {
                    _logger.LogError("EpicTaskService updating JobToEpicTask failed because of invalid jobId or epicTaskId");
                    throw new EpicTaskServiceException("No such jobId or epicTaskId");
                }
            }
            catch (EpicTaskHttpServiceException epicTaskHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService updating JobToEpicTask failled because of " +
                    $"EpicTaskHttpService error: {epicTaskHttpServiceEx.Message}");
                throw new EpicTaskServiceException(epicTaskHttpServiceEx.Message);
            }
            catch (JobHttpServiceException jobHttpServiceEx)
            {
                _logger.LogError($"EpicTaskService updating JobToEpicTask failled because of " +
                    $"JobHttpService error: {jobHttpServiceEx.Message}");
                throw new EpicTaskServiceException(jobHttpServiceEx.Message);

            }
        }

        #endregion
    }
}
