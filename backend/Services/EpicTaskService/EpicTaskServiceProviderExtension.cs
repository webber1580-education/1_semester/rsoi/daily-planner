﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace EpicTaskService
{
    public static class EpicTaskServiceProviderExtensions
    {
        public static void AddEpicTaskService(this IServiceCollection services)
        {
            services.AddSingleton<IEpicTaskService, EpicTaskService>();
        }
    }
}
