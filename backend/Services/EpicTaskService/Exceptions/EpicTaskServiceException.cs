﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpicTaskService.Exceptions
{
    [Serializable]
    public class EpicTaskServiceException : Exception
    {
        public EpicTaskServiceException()
            : base()
        {
        }

        public EpicTaskServiceException(string message)
            : base(message)
        {
        }

        public EpicTaskServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
