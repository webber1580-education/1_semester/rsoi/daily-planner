﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DailyPlanService.Exceptions
{
    [Serializable]
    public class DailyPlanServiceException : Exception
    {
        public DailyPlanServiceException()
            : base()
        {
        }

        public DailyPlanServiceException(string message)
            : base(message)
        {
        }

        public DailyPlanServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
