﻿using HttpRequest;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DailyPlanDatabase.Models;
using DailyPlanService.Exceptions;
using DailyPlanHttpService;
using JobHttpService;
using System.Linq;
using EpicTaskHttpService.Exceptions;
using DailyPlanHttpService.Exceptions;
using JobHttpService.Exceptions;
using UserHttpService;
using UserHttpService.Exceptions;
using Common.ExceptionHandler;

namespace DailyPlanService
{
    public class DailyPlanService : IDailyPlanService
    {
        private readonly ILogger<DailyPlanService> _logger;
        private readonly IDailyPlanHttpService _dailyPlanHttpService;
        private readonly IJobHttpService _jobHttpService;
        private readonly IUserHttpService _userHttpService;

        public DailyPlanService(ILogger<DailyPlanService> logger,
            IDailyPlanHttpService dailyPlanHttpService,
            IJobHttpService jobHttpService,
            IUserHttpService userHttpService)
        {
            _logger = logger;
            _dailyPlanHttpService = dailyPlanHttpService;
            _jobHttpService = jobHttpService;
            _userHttpService = userHttpService;
        }

        #region DailyPlan

        public async Task<int?> CreateDailyPlanAsync(DailyPlan dailyPlan)
        {
            int? result = null;

            try
            {
                var userList = await _userHttpService.GetUsersListAsync();

                if (userList.FirstOrDefault(u => u.Id == dailyPlan.UserId) != null)
                    result = await _dailyPlanHttpService.CreateDailyPlanAsync(dailyPlan);
                else
                {
                    _logger.LogError("DailyPlanService creating DailyPlan failed because of userId");
                    throw new DailyPlanServiceException("No such userId");
                }
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService creating DailyPlan failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new DailyPlanServiceException(userHttpServiceEx.Message);
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService creating DailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                throw new DailyPlanServiceException(dailyPlanHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<DailyPlan> GetDailyPlanAsync(int dailyPlanId)
        {
            DailyPlan result = null;

            try
            {
                result = await _dailyPlanHttpService.GetDailyPlanAsync(dailyPlanId);
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService getting DailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                throw new DailyPlanServiceException(dailyPlanHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<List<DailyPlan>> GetDailyPlansListAsync()
        {
            List<DailyPlan> result = null;

            try
            {
                result = await _dailyPlanHttpService.GetDailyPlansListAsync();
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService getting list of DailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                throw new DailyPlanServiceException(dailyPlanHttpServiceEx.Message);
            }

            return result;
        }

        public async Task DeleteDailyPlanAsync(int dailyPlanId)
        {
            var rollbackOperations = new LinkedList<Func<Task>>();

            try
            {
                var jobToDailyPlanList = await _dailyPlanHttpService.GetJobToDailyPlansListAsync();

                foreach (var jobToDailyPlan in jobToDailyPlanList.Where(j => j.DailyPlanId == dailyPlanId))
                {
                    await _dailyPlanHttpService.DeleteJobToDailyPlanAsync(jobToDailyPlan.Id);
                    rollbackOperations.AddFirst(async () => { await _dailyPlanHttpService.CreateJobToDailyPlanAsync(jobToDailyPlan); });
                }
                await _dailyPlanHttpService.DeleteDailyPlanAsync(dailyPlanId);
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {

                _logger.LogError($"DailyPlanService deleting DailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                var dailyPlanServiceEx = new DailyPlanServiceException(dailyPlanHttpServiceEx.Message); ;
                await ExceptionHandler.RollBackAndThrowAsync(rollbackOperations, dailyPlanServiceEx);
            }
        }

        public async Task UpdateDailyPlanAsync(DailyPlan dailyPlan)
        {
            try
            {
                var userList = await _userHttpService.GetUsersListAsync();

                if (userList.FirstOrDefault(u => u.Id == dailyPlan.UserId) != null)
                    await _dailyPlanHttpService.UpdateDailyPlanAsync(dailyPlan);
                else
                {
                    _logger.LogError("DailyPlanService updating DailyPlan failed because of userId");
                    throw new DailyPlanServiceException("No such userId");
                }
            }
            catch (UserHttpServiceException userHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService updating DailyPlan failled because of " +
                    $"UserHttpService error: {userHttpServiceEx.Message}");
                throw new DailyPlanServiceException(userHttpServiceEx.Message);
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService updating DailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                throw new DailyPlanServiceException(dailyPlanHttpServiceEx.Message);
            }
        }

        #endregion

        #region JobToDailyPlan

        public async Task<int?> CreateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan)
        {
            int? result = null;

            try
            {
                var jobToDailyPlanList = await _dailyPlanHttpService.GetJobToDailyPlansListAsync();
                var dailyPlanList = await _dailyPlanHttpService.GetDailyPlansListAsync();
                var jobList = await _jobHttpService.GetJobsListAsync();

                if (jobToDailyPlanList.FirstOrDefault(j => j.JobId == jobToDailyPlan.JobId 
                        && j.DailyPlanId == jobToDailyPlan.DailyPlanId) != null)
                {
                    _logger.LogError("DailyPlanService creating JobToDailyPlan failed because it's already exists");
                    throw new DailyPlanServiceException("JobToDailyPlan is already exists");
                }
                else if (dailyPlanList.FirstOrDefault(d => d.Id == jobToDailyPlan.DailyPlanId) != null
                            && jobList.FirstOrDefault(j => j.Id == jobToDailyPlan.JobId) != null)
                    result = await _dailyPlanHttpService.CreateJobToDailyPlanAsync(jobToDailyPlan);
                else
                {
                    _logger.LogError("DailyPlanService creating JobToDailyPlan failed because of invalid jobId or dailyPlanId");
                    throw new DailyPlanServiceException("No such jobId or dailyPlanId");
                }
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService creating JobToDailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                throw new DailyPlanServiceException(dailyPlanHttpServiceEx.Message);
            }
            catch (JobHttpServiceException jobHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService creating JobToDailyPlan failled because of " +
                    $"JobHttpService error: {jobHttpServiceEx.Message}");
                throw new DailyPlanServiceException(jobHttpServiceEx.Message);

            }

            return result;
        }

        public async Task<JobToDailyPlan> GetJobToDailyPlanAsync(int jobToDailyPlanId)
        {
            JobToDailyPlan result = null;

            try
            {
                result = await _dailyPlanHttpService.GetJobToDailyPlanAsync(jobToDailyPlanId);
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService getting JobToDailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                throw new DailyPlanServiceException(dailyPlanHttpServiceEx.Message);
            }

            return result;
        }

        public async Task<List<JobToDailyPlan>> GetJobToDailyPlansListAsync()
        {
            List<JobToDailyPlan> result = null;

            try
            {
                result = await _dailyPlanHttpService.GetJobToDailyPlansListAsync();
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService getting list of JobToDailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                throw new DailyPlanServiceException(dailyPlanHttpServiceEx.Message);
            }

            return result;
        }

        public async Task DeleteJobToDailyPlanAsync(int jobToDailyPlanId)
        {
            try
            {
                await _dailyPlanHttpService.DeleteJobToDailyPlanAsync(jobToDailyPlanId);
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService deleting JobToDailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                throw new DailyPlanServiceException(dailyPlanHttpServiceEx.Message);
            }
        }

        public async Task UpdateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan)
        {
            try
            {
                var jobToDailyPlanList = await _dailyPlanHttpService.GetJobToDailyPlansListAsync();
                var dailyPlanList = await _dailyPlanHttpService.GetDailyPlansListAsync();
                var jobList = await _jobHttpService.GetJobsListAsync();

                if (jobToDailyPlanList.FirstOrDefault(j => j.JobId == jobToDailyPlan.JobId
                        && j.DailyPlanId == jobToDailyPlan.DailyPlanId) != null)
                {
                    _logger.LogError("DailyPlanService updating JobToDailyPlan failed because it's already exists");
                    throw new DailyPlanServiceException("JobToDailyPlan is already exists");
                }
                else if (dailyPlanList.FirstOrDefault(d => d.Id == jobToDailyPlan.DailyPlanId) != null
                            && jobList.FirstOrDefault(j => j.Id == jobToDailyPlan.JobId) != null)
                    await _dailyPlanHttpService.UpdateJobToDailyPlanAsync(jobToDailyPlan);
                else
                {
                    _logger.LogError("DailyPlanService updating JobToDailyPlan failed because of invalid jobId or dailyPlanId");
                    throw new DailyPlanServiceException("No such jobId or dailyPlanId");
                }
            }
            catch (DailyPlanHttpServiceException dailyPlanHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService updating JobToDailyPlan failled because of " +
                    $"DailyPlanHttpService error: {dailyPlanHttpServiceEx.Message}");
                throw new DailyPlanServiceException(dailyPlanHttpServiceEx.Message);
            }
            catch (JobHttpServiceException jobHttpServiceEx)
            {
                _logger.LogError($"DailyPlanService updating JobToDailyPlan failled because of " +
                    $"JobHttpService error: {jobHttpServiceEx.Message}");
                throw new DailyPlanServiceException(jobHttpServiceEx.Message);

            }
        }

        #endregion
    }
}
