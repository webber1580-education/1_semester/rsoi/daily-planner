﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace DailyPlanService
{
    public static class DailyPlanServiceProviderExtensions
    {
        public static void AddDailyPlanService(this IServiceCollection services)
        {
            services.AddSingleton<IDailyPlanService, DailyPlanService>();
        }
    }
}
