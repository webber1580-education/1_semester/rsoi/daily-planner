﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpicTaskDatabase.Models
{
    public class EpicTask
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int UserId { get; set; }
    }
}
