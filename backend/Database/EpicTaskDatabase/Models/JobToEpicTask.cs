﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpicTaskDatabase.Models
{
    public class JobToEpicTask
    {
        public int Id { get; set; }

        public int JobId { get; set; }

        public int EpicTaskId { get; set; }
    }
}
