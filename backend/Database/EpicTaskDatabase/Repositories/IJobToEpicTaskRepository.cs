﻿using EpicTaskDatabase.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EpicTaskDatabase.Repository
{
    public interface IJobToEpicTaskRepository
    {
        Task CreateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask);

        Task<List<JobToEpicTask>> GetJobToEpicTasksListAsync();

        Task<JobToEpicTask> GetJobToEpicTaskAsync(int id);

        Task UpdateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask);

        Task DeleteJobToEpicTaskAsync(int id);

    }
}
