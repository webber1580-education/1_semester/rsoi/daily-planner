﻿using EpicTaskDatabase.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpicTaskDatabase.Repository
{
    public class JobToEpicTaskRepository : IJobToEpicTaskRepository
    {
        private EpicTaskDbContext _jobToEpicTaskContext;

        public JobToEpicTaskRepository(EpicTaskDbContext jobToEpicTaskContext)
        {
            _jobToEpicTaskContext = jobToEpicTaskContext;
        }

        public async Task CreateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask)
        {
            var jobToEpicTaskLast = await _jobToEpicTaskContext.JobToEpicTasks.OrderBy(x => x.Id).LastOrDefaultAsync();
            jobToEpicTask.Id = jobToEpicTaskLast != null ? jobToEpicTaskLast.Id + 1 : 1;
            await _jobToEpicTaskContext.JobToEpicTasks.AddAsync(jobToEpicTask);
            await _jobToEpicTaskContext.SaveChangesAsync();
        }

        public async Task<List<JobToEpicTask>> GetJobToEpicTasksListAsync()
        {
            return await _jobToEpicTaskContext.JobToEpicTasks.OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<JobToEpicTask> GetJobToEpicTaskAsync(int id)
        {
            try
            {
                return await _jobToEpicTaskContext.JobToEpicTasks.FirstAsync(d => d.Id == id);
            }
            catch (Exception)
            {
                throw new Exception("No JobToEpicTask with such Id");
            }
        }

        public async Task UpdateJobToEpicTaskAsync(JobToEpicTask jobToEpicTask)
        {
            try
            {
                var updatedJobToEpicTask = await _jobToEpicTaskContext.JobToEpicTasks.FirstAsync(d => d.Id == jobToEpicTask.Id);
                updatedJobToEpicTask.EpicTaskId= jobToEpicTask.EpicTaskId;
                updatedJobToEpicTask.JobId = jobToEpicTask.JobId;

                _jobToEpicTaskContext.JobToEpicTasks.Update(updatedJobToEpicTask);
                await _jobToEpicTaskContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No JobToEpicTask with such Id");
            }
        }

        public async Task DeleteJobToEpicTaskAsync(int id)
        {
            try
            {
                var removePlan = await _jobToEpicTaskContext.JobToEpicTasks.FirstAsync(d => d.Id == id);
                _jobToEpicTaskContext.JobToEpicTasks.Remove(removePlan);
                await _jobToEpicTaskContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No JobToEpicTask with such Id");
            }
        }
    }
}
