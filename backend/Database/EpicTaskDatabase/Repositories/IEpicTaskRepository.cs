﻿using EpicTaskDatabase.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EpicTaskDatabase.Repository
{
    public interface IEpicTaskRepository
    {
        Task CreateEpicTaskAsync(EpicTask epicTask);

        Task<List<EpicTask>> GetEpicTasksListAsync();

        Task<EpicTask> GetEpicTaskAsync(int id);

        Task UpdateEpicTaskAsync(EpicTask epicTask);

        Task DeleteEpicTaskAsync(int id);

    }
}
