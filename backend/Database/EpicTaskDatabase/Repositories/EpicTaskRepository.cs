﻿using EpicTaskDatabase.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpicTaskDatabase.Repository
{
    public class EpicTaskRepository : IEpicTaskRepository
    {
        private EpicTaskDbContext _epicTaskContext;

        public EpicTaskRepository(EpicTaskDbContext epicTaskContext)
        {
            _epicTaskContext = epicTaskContext;
        }

        public async Task CreateEpicTaskAsync(EpicTask epicTask)
        {
            var epicTaskLast = await _epicTaskContext.EpicTasks.OrderBy(x => x.Id).LastOrDefaultAsync();
            epicTask.Id = epicTaskLast != null ? epicTaskLast.Id + 1 : 1;
            await _epicTaskContext.EpicTasks.AddAsync(epicTask);
            await _epicTaskContext.SaveChangesAsync();
        }

        public async Task<List<EpicTask>> GetEpicTasksListAsync()
        {
            return await _epicTaskContext.EpicTasks.OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<EpicTask> GetEpicTaskAsync(int id)
        {
            try
            {
                return await _epicTaskContext.EpicTasks.FirstAsync(d => d.Id == id);
            }
            catch (Exception)
            {
                throw new Exception("No EpicTask with such Id");
            }
        }

        public async Task UpdateEpicTaskAsync(EpicTask epicTask)
        {
            try
            {
                var updatedEpicTask = await _epicTaskContext.EpicTasks.FirstAsync(d => d.Id == epicTask.Id);
                updatedEpicTask.Title = epicTask.Title;
                updatedEpicTask.Description = epicTask.Description;
                updatedEpicTask.UserId = epicTask.UserId;

                _epicTaskContext.EpicTasks.Update(updatedEpicTask);
                await _epicTaskContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No EpicTask with such Id");
            }
        }

        public async Task DeleteEpicTaskAsync(int id)
        {
            try
            {
                var removePlan = await _epicTaskContext.EpicTasks.FirstAsync(d => d.Id == id);
                _epicTaskContext.EpicTasks.Remove(removePlan);
                await _epicTaskContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No EpicTask with such Id");
            }
        }
    }
}
