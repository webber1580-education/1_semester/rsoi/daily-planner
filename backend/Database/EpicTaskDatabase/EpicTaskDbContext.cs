﻿using EpicTaskDatabase.Models;
using Microsoft.EntityFrameworkCore;

namespace EpicTaskDatabase
{
    public class EpicTaskDbContext : DbContext
    {
        public EpicTaskDbContext(DbContextOptions<EpicTaskDbContext> options) : base(options) { }

        public DbSet<EpicTask> EpicTasks { get; set; }
        public DbSet<JobToEpicTask> JobToEpicTasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("epicTask-db");

            // primary keys
            modelBuilder.Entity<EpicTask>().HasKey(d => d.Id);
            modelBuilder.Entity<JobToEpicTask>().HasKey(j => j.Id);
        }

    }
}
