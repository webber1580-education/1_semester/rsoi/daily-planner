﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobDatabase.Models
{
    public class Job
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int PreorityNumber { get; set; }

        public string Description { get; set; }

        public int UserId { get; set; }
    }
}
