﻿using JobDatabase.Models;
using Microsoft.EntityFrameworkCore;

namespace JobDatabase
{
    public class JobDbContext : DbContext
    {
        public JobDbContext(DbContextOptions<JobDbContext> options) : base(options) { }

        public DbSet<Job> Jobs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("job-db");

            // primary keys
            modelBuilder.Entity<Job>().HasKey(d => d.Id);
        }

    }
}
