﻿using JobDatabase.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobDatabase.Repository
{
    public class JobRepository : IJobRepository
    {
        private JobDbContext _jobContext;

        public JobRepository(JobDbContext jobContext)
        {
            _jobContext = jobContext;
        }

        public async Task CreateJobAsync(Job job)
        {
            var jobLast = await _jobContext.Jobs.OrderBy(x => x.Id).LastOrDefaultAsync();
            job.Id = jobLast != null ? jobLast.Id + 1 : 1;
            await _jobContext.Jobs.AddAsync(job);
            await _jobContext.SaveChangesAsync();
        }

        public async Task<List<Job>> GetJobsListAsync()
        {
            return await _jobContext.Jobs.OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<Job> GetJobAsync(int id)
        {
            try
            {
                return await _jobContext.Jobs.FirstAsync(d => d.Id == id);
            }
            catch (Exception)
            {
                throw new Exception("No Job with such Id");
            }
        }

        public async Task UpdateJobAsync(Job job)
        {
            try
            {
                var updatedJob = await _jobContext.Jobs.FirstAsync(d => d.Id == job.Id);
                updatedJob.PreorityNumber = job.PreorityNumber;
                updatedJob.Description = job.Description;
                updatedJob.Title = job.Title;
                updatedJob.UserId = job.UserId;

                _jobContext.Jobs.Update(updatedJob);
                await _jobContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No Job with such Id");
            }
        }

        public async Task DeleteJobAsync(int id)
        {
            try
            {
                var removePlan = await _jobContext.Jobs.FirstAsync(d => d.Id == id);
                _jobContext.Jobs.Remove(removePlan);
                await _jobContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No Job with such Id");
            }
        }
    }
}
