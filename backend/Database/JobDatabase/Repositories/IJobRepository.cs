﻿using JobDatabase.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobDatabase.Repository
{
    public interface IJobRepository
    {
        Task CreateJobAsync(Job job);

        Task<List<Job>> GetJobsListAsync();

        Task<Job> GetJobAsync(int id);

        Task UpdateJobAsync(Job job);

        Task DeleteJobAsync(int id);
    }
}
