﻿using UserDatabase.Models;
using Microsoft.EntityFrameworkCore;

namespace UserDatabase
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("user-db");

            // primary keys
            modelBuilder.Entity<User>().HasKey(d => d.Id);

            // data types 
            modelBuilder.Entity<User>()
                .Property(d => d.DateOfBirth).HasColumnType("date");
        }

    }
}
