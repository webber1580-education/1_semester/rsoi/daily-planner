﻿using UserDatabase.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace UserDatabase.Repository
{
    public interface IUserRepository
    {
        Task CreateUserAsync(User user);

        Task<List<User>> GetUsersListAsync();

        Task<User> GetUserAsync(int id);

        Task UpdateUserAsync(User user);

        Task DeleteUserAsync(int id);

    }
}
