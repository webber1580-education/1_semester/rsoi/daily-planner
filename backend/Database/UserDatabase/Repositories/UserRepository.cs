﻿using UserDatabase.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace UserDatabase.Repository
{
    public class UserRepository : IUserRepository
    {
        private UserDbContext _userContext;

        public UserRepository(UserDbContext userContext)
        {
            _userContext = userContext;
        }

        public async Task CreateUserAsync(User user)
        {
            var userLast = await _userContext.Users.OrderBy(x => x.Id).LastOrDefaultAsync();
            user.Id = userLast != null ? userLast.Id + 1 : 1;
            await _userContext.Users.AddAsync(user);
            await _userContext.SaveChangesAsync();
        }

        public async Task<List<User>> GetUsersListAsync()
        {
            return await _userContext.Users.OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<User> GetUserAsync(int id)
        {
            try
            {
                return await _userContext.Users.FirstAsync(d => d.Id == id);
            }
            catch (Exception)
            {
                throw new Exception("No User with such Id");
            }
        }

        public async Task UpdateUserAsync(User user)
        {
            try
            {
                var updatedUser = await _userContext.Users.FirstAsync(d => d.Id == user.Id);
                updatedUser.Name = user.Name;
                updatedUser.Surname = user.Surname;
                updatedUser.DateOfBirth = user.DateOfBirth;

                _userContext.Users.Update(updatedUser);
                await _userContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No User with such Id");
            }
        }

        public async Task DeleteUserAsync(int id)
        {
            try
            {
                var removePlan = await _userContext.Users.FirstAsync(d => d.Id == id);
                _userContext.Users.Remove(removePlan);
                await _userContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No User with such Id");
            }
        }
    }
}
