﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DailyPlanDatabase.Models
{
    public class DailyPlan
    {
        public int Id { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public DateTime CurrentDate { get; set; }

        public int UserId { get; set; }
    }

}
