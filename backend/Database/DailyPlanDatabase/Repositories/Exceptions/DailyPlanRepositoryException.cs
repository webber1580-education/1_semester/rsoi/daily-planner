﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DailyPlanDatabase.Repositories.Exceptions
{
    [Serializable]
    public class DailyPlanRepositoryException : Exception
    {
        public DailyPlanRepositoryException()
            : base()
        {
        }

        public DailyPlanRepositoryException(string message)
            : base(message)
        {
        }

        public DailyPlanRepositoryException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
