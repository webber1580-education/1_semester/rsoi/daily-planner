﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DailyPlanDatabase.Repositories.Exceptions
{
    [Serializable]
    public class JobToDailyPlanRepositoryException : Exception
    {
        public JobToDailyPlanRepositoryException()
            : base()
        {
        }

        public JobToDailyPlanRepositoryException(string message)
            : base(message)
        {
        }

        public JobToDailyPlanRepositoryException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
