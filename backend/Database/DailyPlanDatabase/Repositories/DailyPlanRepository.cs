﻿using DailyPlanDatabase.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyPlanDatabase.Repository
{
    public class DailyPlanRepository : IDailyPlanRepository
    {
        private DailyPlanDbContext _dailyPlanContext;

        public DailyPlanRepository(DailyPlanDbContext dailyPlanContext)
        {
            _dailyPlanContext = dailyPlanContext;
        }

        public async Task CreateDailyPlanAsync(DailyPlan dailyPlan)
        {
            var dailyPlanLast = await _dailyPlanContext.DailyPlans.OrderBy(x => x.Id).LastOrDefaultAsync();
            dailyPlan.Id = dailyPlanLast != null? dailyPlanLast.Id + 1 : 1;
            await _dailyPlanContext.DailyPlans.AddAsync(dailyPlan);
            await _dailyPlanContext.SaveChangesAsync();
        }

        public async Task<List<DailyPlan>> GetDailyPlansListAsync()
        {
            return await _dailyPlanContext.DailyPlans.OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<DailyPlan> GetDailyPlanAsync(int id)
        {
            try
            {
                return await _dailyPlanContext.DailyPlans.FirstAsync(d => d.Id == id);
            }
            catch (Exception)
            {
                throw new Exception("No DailyPlan with such Id");
            }
        }

        public async Task UpdateDailyPlanAsync(DailyPlan dailyPlan)
        {
            try
            {
                var updatedDailyPlan = await _dailyPlanContext.DailyPlans.FirstAsync(d => d.Id == dailyPlan.Id);
                updatedDailyPlan.DayOfWeek = dailyPlan.DayOfWeek;
                updatedDailyPlan.CurrentDate = dailyPlan.CurrentDate;
                updatedDailyPlan.UserId = dailyPlan.UserId;

                _dailyPlanContext.DailyPlans.Update(updatedDailyPlan);
                await _dailyPlanContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No DailyPlan with such Id");
            }
        }

        public async Task DeleteDailyPlanAsync(int id)
        {
            try
            {
                var removePlan = await _dailyPlanContext.DailyPlans.FirstAsync(d => d.Id == id);
                _dailyPlanContext.DailyPlans.Remove(removePlan);
                await _dailyPlanContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No DailyPlan with such Id");
            }
        }
    }
}
