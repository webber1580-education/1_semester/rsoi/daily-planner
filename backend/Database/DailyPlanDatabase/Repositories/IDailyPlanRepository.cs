﻿using DailyPlanDatabase.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DailyPlanDatabase.Repository
{
    public interface IDailyPlanRepository
    {
        Task CreateDailyPlanAsync(DailyPlan dailyPlan);

        Task<List<DailyPlan>> GetDailyPlansListAsync();

        Task<DailyPlan> GetDailyPlanAsync(int id);

        Task UpdateDailyPlanAsync(DailyPlan dailyPlan);

        Task DeleteDailyPlanAsync(int id);

    }
}
