﻿using DailyPlanDatabase.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DailyPlanDatabase.Repository
{
    public interface IJobToDailyPlanRepository
    {
        Task CreateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan);

        Task<List<JobToDailyPlan>> GetJobToDailyPlansListAsync();

        Task<JobToDailyPlan> GetJobToDailyPlanAsync(int id);

        Task UpdateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan);

        Task DeleteJobToDailyPlanAsync(int id);

    }
}
