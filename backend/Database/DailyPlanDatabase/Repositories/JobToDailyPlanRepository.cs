﻿using DailyPlanDatabase.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyPlanDatabase.Repository
{
    public class JobToDailyPlanRepository : IJobToDailyPlanRepository
    {
        private DailyPlanDbContext _jobToDailyPlanContext;

        public JobToDailyPlanRepository(DailyPlanDbContext jobToDailyPlanContext)
        {
            _jobToDailyPlanContext = jobToDailyPlanContext;
        }

        public async Task CreateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan)
        {
            var jobToDailyPlanLast = await _jobToDailyPlanContext.JobToDailyPlans.OrderBy(x => x.Id).LastOrDefaultAsync();
            jobToDailyPlan.Id = jobToDailyPlanLast != null ? jobToDailyPlanLast.Id + 1 : 1;
            await _jobToDailyPlanContext.JobToDailyPlans.AddAsync(jobToDailyPlan);
            await _jobToDailyPlanContext.SaveChangesAsync();
        }

        public async Task<List<JobToDailyPlan>> GetJobToDailyPlansListAsync()
        {
            return await _jobToDailyPlanContext.JobToDailyPlans.OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<JobToDailyPlan> GetJobToDailyPlanAsync(int id)
        {
            try
            {
                return await _jobToDailyPlanContext.JobToDailyPlans.FirstAsync(d => d.Id == id);
            }
            catch (Exception)
            {
                throw new Exception("No JobToDailyPlan with such Id");
            }
        }

        public async Task UpdateJobToDailyPlanAsync(JobToDailyPlan jobToDailyPlan)
        {
            try
            {
                var updatedJobToDailyPlan = await _jobToDailyPlanContext.JobToDailyPlans
                    .FirstAsync(d => d.Id == jobToDailyPlan.Id);
                updatedJobToDailyPlan.DailyPlanId = jobToDailyPlan.DailyPlanId;
                updatedJobToDailyPlan.JobId = jobToDailyPlan.JobId;

                _jobToDailyPlanContext.JobToDailyPlans.Update(updatedJobToDailyPlan);
                await _jobToDailyPlanContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No JobToDailyPlan with such Id");
            }
        }

        public async Task DeleteJobToDailyPlanAsync(int id)
        {
            try
            {
                var removePlan = await _jobToDailyPlanContext.JobToDailyPlans.FirstAsync(d => d.Id == id);
                _jobToDailyPlanContext.JobToDailyPlans.Remove(removePlan);
                await _jobToDailyPlanContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new Exception("No JobToDailyPlan with such Id");
            }
        }
    }
}
