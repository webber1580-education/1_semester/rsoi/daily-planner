﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobToDailyPlanDatabase.Models
{
    public class JobToDailyPlan
    {
        public int Id { get; set; }

        public int JobId { get; set; }

        public int DailyPlanId { get; set; }
    }
}
