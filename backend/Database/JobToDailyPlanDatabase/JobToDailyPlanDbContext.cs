﻿using DailyPlanDatabase.Models;
using Microsoft.EntityFrameworkCore;

namespace DailyPlanDatabase
{
    public class DailyPlanDbContext : DbContext
    {
        public DailyPlanDbContext(DbContextOptions<DailyPlanDbContext> options) : base(options) { }

        public DbSet<DailyPlan> DailyPlans { get; set; }
        public DbSet<JobToDailyPlan> JobToDailyPlans { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dailyPlan-db");

            // primary keys
            modelBuilder.Entity<DailyPlan>().HasKey(d => d.Id);
            modelBuilder.Entity<JobToDailyPlan>().HasKey(j => j.Id);

            // data types 
            modelBuilder.Entity<DailyPlan>()
                .Property(d => d.CurrentDate).HasColumnType("date");
        }

    }
}
