using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JobWebService.Controllers;
using JobDatabase.Models;
using JobDatabase.Repository;
using Microsoft.Extensions.Logging;

namespace Test.JobWebService
{
    [TestClass]
    public class JobTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultJobId = 1;

        #region Ok

        [TestMethod]
        public async Task CreateJobOk()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            var result = (ObjectResult)await controller.CreateJob(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
            Assert.AreEqual(result.Value, _defaultJobId);
        }

        [TestMethod]
        public async Task GetJobListOk()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobsListAsync()).Returns(Task.FromResult(new List<Job>()
            {
                new Job() { Id = 1, PreorityNumber = 1, Description = "1", Title = "1", UserId = 1},
                new Job() { Id = 2, PreorityNumber = 2, Description = "2", Title = "2", UserId = 2},
                new Job() { Id = 3, PreorityNumber = 3, Description = "3", Title = "3", UserId = 3},
            }));

            var result = (ObjectResult)await controller.GetJobsList();
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetJobOk()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobAsync(It.IsAny<int>())).Returns(Task.FromResult(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            }));

            var result = (ObjectResult)await controller.GetJob(_defaultJobId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateJobOk()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.UpdateJob(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteJobOk()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.DeleteJob(_defaultJobId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateJobError()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            repository.Setup(r => r.CreateJobAsync(It.IsAny<Job>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateJob(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetJobListError()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobsListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobsList();
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetJobError()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJob(_defaultJobId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateJobError()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            repository.Setup(r => r.UpdateJobAsync(It.IsAny<Job>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateJob(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteJobError()
        {
            var repository = new Mock<IJobRepository>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, repository.Object);

            repository.Setup(r => r.DeleteJobAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteJob(_defaultJobId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
