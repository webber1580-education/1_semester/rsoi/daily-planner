﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JobToEpicTaskWebService.Controllers;
using EpicTaskDatabase.Models;
using EpicTaskDatabase.Repository;
using Microsoft.Extensions.Logging;

namespace Test.JobToEpicTaskWebService
{
    [TestClass]
    public class JobToEpicTasks
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultJobToEpicTaskId = 1;

        #region Ok

        [TestMethod]
        public async Task CreateJobToEpicTaskOk()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            var result = (ObjectResult)await controller.CreateJobToEpicTask(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
            Assert.AreEqual(result.Value, _defaultJobToEpicTaskId);
        }

        [TestMethod]
        public async Task GetJobToEpicTaskListOk()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobToEpicTasksListAsync()).Returns(Task.FromResult(new List<JobToEpicTask>()
            {
                new JobToEpicTask() { Id = 1, EpicTaskId = 1, JobId = 1 },
                new JobToEpicTask() { Id = 2, EpicTaskId = 2, JobId = 2 },
                new JobToEpicTask() { Id = 3, EpicTaskId = 3, JobId = 3 },
            }));

            var result = (ObjectResult)await controller.GetJobToEpicTasksList();
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetJobToEpicTaskOk()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobToEpicTaskAsync(It.IsAny<int>())).Returns(Task.FromResult(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            }));

            var result = (ObjectResult)await controller.GetJobToEpicTask(_defaultJobToEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateJobToEpicTaskOk()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.UpdateJobToEpicTask(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteJobToEpicTaskOk()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.DeleteJobToEpicTask(_defaultJobToEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateJobToEpicTaskError()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.CreateJobToEpicTaskAsync(It.IsAny<JobToEpicTask>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateJobToEpicTask(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetJobToEpicTaskListError()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobToEpicTasksListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobToEpicTasksList();
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetJobToEpicTaskError()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobToEpicTaskAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobToEpicTask(_defaultJobToEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateJobToEpicTaskError()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.UpdateJobToEpicTaskAsync(It.IsAny<JobToEpicTask>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateJobToEpicTask(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteJobToEpicTaskError()
        {
            var repository = new Mock<IJobToEpicTaskRepository>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.DeleteJobToEpicTaskAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteJobToEpicTask(_defaultJobToEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
