﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EpicTaskWebService.Controllers;
using EpicTaskDatabase.Models;
using EpicTaskDatabase.Repository;
using Microsoft.Extensions.Logging;

namespace Test.EpicTaskWebService
{
    [TestClass]
    public class EpicTaskTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultEpicTaskId = 1;

        #region Ok

        [TestMethod]
        public async Task CreateEpicTaskOk()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            var result = (ObjectResult)await controller.CreateEpicTask(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
            Assert.AreEqual(result.Value, _defaultEpicTaskId);
        }

        [TestMethod]
        public async Task GetEpicTaskListOk()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.GetEpicTasksListAsync()).Returns(Task.FromResult(new List<EpicTask>()
            {
                new EpicTask() { Id = 1, Title = "1", Description = "1", UserId = 1},
                new EpicTask() { Id = 1, Title = "2", Description = "2", UserId = 2},
                new EpicTask() { Id = 1, Title = "3", Description = "3", UserId = 3},
            }));

            var result = (ObjectResult)await controller.GetEpicTasksList();
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetEpicTaskOk()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.GetEpicTaskAsync(It.IsAny<int>())).Returns(Task.FromResult(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            }));

            var result = (ObjectResult)await controller.GetEpicTask(_defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateEpicTaskOk()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.UpdateEpicTask(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteEpicTaskOk()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.DeleteEpicTask(_defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateEpicTaskError()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.CreateEpicTaskAsync(It.IsAny<EpicTask>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateEpicTask(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetEpicTaskListError()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.GetEpicTasksListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetEpicTasksList();
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetEpicTaskError()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.GetEpicTaskAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetEpicTask(_defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateEpicTaskError()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.UpdateEpicTaskAsync(It.IsAny<EpicTask>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateEpicTask(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteEpicTaskError()
        {
            var repository = new Mock<IEpicTaskRepository>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, repository.Object);

            repository.Setup(r => r.DeleteEpicTaskAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteEpicTask(_defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
