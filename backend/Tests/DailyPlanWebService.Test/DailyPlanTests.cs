using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DailyPlanWebService.Controllers;
using DailyPlanDatabase.Models;
using DailyPlanDatabase.Repository;
using Microsoft.Extensions.Logging;

namespace Test.DailyPlanWebService
{
    [TestClass]
    public class DailyPlanTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultDailyPlanId = 1;

        #region Ok

        [TestMethod]
        public async Task CreateDailyPlanOk()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            var result = (ObjectResult)await controller.CreateDailyPlan(new DailyPlan()
            {
                Id = 1,
                UserId = 1, 
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
            Assert.AreEqual(result.Value, _defaultDailyPlanId);
        }

        [TestMethod]
        public async Task GetDailyPlanListOk()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.GetDailyPlansListAsync()).Returns(Task.FromResult(new List<DailyPlan>()
            {
                new DailyPlan() { Id = 1, UserId = 1, CurrentDate = DateTime.Now, DayOfWeek = DayOfWeek.Monday},
                new DailyPlan() { Id = 2, UserId = 2, CurrentDate = DateTime.Now, DayOfWeek = DayOfWeek.Tuesday},
                new DailyPlan() { Id = 3, UserId = 3, CurrentDate = DateTime.Now, DayOfWeek = DayOfWeek.Wednesday},
            }));

            var result = (ObjectResult)await controller.GetDailyPlansList();
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetDailyPlanOk()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.GetDailyPlanAsync(It.IsAny<int>())).Returns(Task.FromResult(new DailyPlan()
            {
                Id = 1,
                UserId = 1,
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            }));

            var result = (ObjectResult)await controller.GetDailyPlan(_defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateDailyPlanOk()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.UpdateDailyPlan(new DailyPlan()
            {
                Id = 1,
                UserId = 1,
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteDailyPlanOk()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.DeleteDailyPlan(_defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateDailyPlanError()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.CreateDailyPlanAsync(It.IsAny<DailyPlan>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateDailyPlan(new DailyPlan()
            {
                Id = 1,
                UserId = 1,
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetDailyPlanListError()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.GetDailyPlansListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetDailyPlansList();
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetDailyPlanError()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.GetDailyPlanAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetDailyPlan(_defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateDailyPlanError()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.UpdateDailyPlanAsync(It.IsAny<DailyPlan>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateDailyPlan(new DailyPlan()
            {
                Id = 1,
                UserId = 1,
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteDailyPlanError()
        {
            var repository = new Mock<IDailyPlanRepository>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.DeleteDailyPlanAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteDailyPlan(_defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
