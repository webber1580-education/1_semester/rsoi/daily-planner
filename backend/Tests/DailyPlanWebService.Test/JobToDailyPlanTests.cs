﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JobToDailyPlanWebService.Controllers;
using DailyPlanDatabase.Models;
using DailyPlanDatabase.Repository;
using Microsoft.Extensions.Logging;

namespace Test.JobToDailyPlanWebService
{
    [TestClass]
    public class JobToDailyPlans
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultJobToDailyPlanId = 1;

        #region Ok

        [TestMethod]
        public async Task CreateJobToDailyPlanOk()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            var result = (ObjectResult)await controller.CreateJobToDailyPlan(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
            Assert.AreEqual(result.Value, _defaultJobToDailyPlanId);
        }

        [TestMethod]
        public async Task GetJobToDailyPlanListOk()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobToDailyPlansListAsync()).Returns(Task.FromResult(new List<JobToDailyPlan>()
            {
                new JobToDailyPlan() { Id = 1, DailyPlanId = 1, JobId = 1 },
                new JobToDailyPlan() { Id = 2, DailyPlanId = 2, JobId = 2 },
                new JobToDailyPlan() { Id = 3, DailyPlanId = 3, JobId = 3 },
            }));

            var result = (ObjectResult)await controller.GetJobToDailyPlansList();
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetJobToDailyPlanOk()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobToDailyPlanAsync(It.IsAny<int>())).Returns(Task.FromResult(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            }));

            var result = (ObjectResult)await controller.GetJobToDailyPlan(_defaultJobToDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateJobToDailyPlanOk()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.UpdateJobToDailyPlan(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteJobToDailyPlanOk()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.DeleteJobToDailyPlan(_defaultJobToDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateJobToDailyPlanError()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.CreateJobToDailyPlanAsync(It.IsAny<JobToDailyPlan>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateJobToDailyPlan(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetJobToDailyPlanListError()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobToDailyPlansListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobToDailyPlansList();
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetJobToDailyPlanError()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.GetJobToDailyPlanAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobToDailyPlan(_defaultJobToDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateJobToDailyPlanError()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.UpdateJobToDailyPlanAsync(It.IsAny<JobToDailyPlan>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateJobToDailyPlan(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteJobToDailyPlanError()
        {
            var repository = new Mock<IJobToDailyPlanRepository>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, repository.Object);

            repository.Setup(r => r.DeleteJobToDailyPlanAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteJobToDailyPlan(_defaultJobToDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
