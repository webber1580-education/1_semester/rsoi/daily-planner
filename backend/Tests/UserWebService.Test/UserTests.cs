using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserWebService.Controllers;
using UserDatabase.Models;
using UserDatabase.Repository;
using Microsoft.Extensions.Logging;

namespace Test.UserWebService
{
    [TestClass]
    public class UserTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultUserId = 1;

        #region Ok

        [TestMethod]
        public async Task CreateUserOk()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            var result = (ObjectResult)await controller.CreateUser(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
            Assert.AreEqual(result.Value, _defaultUserId);
        }

        [TestMethod]
        public async Task GetUserListOk()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            repository.Setup(r => r.GetUsersListAsync()).Returns(Task.FromResult(new List<User>()
            {
                new User() { Id = 1, Name = "1", Surname = "1", DateOfBirth = DateTime.Now},
                new User() { Id = 1, Name = "2", Surname = "2", DateOfBirth = DateTime.Now},
                new User() { Id = 1, Name = "3", Surname = "3", DateOfBirth = DateTime.Now},
            }));

            var result = (ObjectResult)await controller.GetUsersList();
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetUserOk()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            repository.Setup(r => r.GetUserAsync(It.IsAny<int>())).Returns(Task.FromResult(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            }));

            var result = (ObjectResult)await controller.GetUser(_defaultUserId); 
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateUserOk()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.UpdateUser(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteUserOk()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            var result = (StatusCodeResult)await controller.DeleteUser(_defaultUserId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateUserError()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            repository.Setup(r => r.CreateUserAsync(It.IsAny<User>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateUser(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetUserListError()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            repository.Setup(r => r.GetUsersListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetUsersList();
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetUserError()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            repository.Setup(r => r.GetUserAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetUser(_defaultUserId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateUserError()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            repository.Setup(r => r.UpdateUserAsync(It.IsAny<User>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateUser(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteUserError()
        {
            var repository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, repository.Object);

            repository.Setup(r => r.DeleteUserAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteUser(_defaultUserId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
