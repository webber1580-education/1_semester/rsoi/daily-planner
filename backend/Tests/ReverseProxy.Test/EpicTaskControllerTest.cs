﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ReverseProxy.Controllers;
using EpicTaskDatabase.Models;
using Microsoft.Extensions.Logging;
using EpicTaskService;
using GatewayService;
using JobDatabase.Models;

namespace ReverseProxy.Test
{
    [TestClass]
    public class EpicTaskTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultJobId = 1;
        private const int _defaultEpicTaskId = 1;
        private const int _from = 1;
        private const int _count = 3;

        #region Ok

        [TestMethod]
        public async Task CreateEpicTaskOk()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.CreateEpicTask(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
        }

        [TestMethod]
        public async Task GetEpicTaskListOk()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            epicTaskService.Setup(r => r.GetEpicTasksListAsync()).Returns(Task.FromResult(new List<EpicTask>()
            {
                new EpicTask() { Id = 1, Title = "1", Description = "1", UserId = 1},
                new EpicTask() { Id = 1, Title = "2", Description = "2", UserId = 2},
                new EpicTask() { Id = 1, Title = "3", Description = "3", UserId = 3},
            }));

            var result = (ObjectResult)await controller.GetEpicTasksList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetEpicTaskOk()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            epicTaskService.Setup(r => r.GetEpicTaskAsync(It.IsAny<int>())).Returns(Task.FromResult(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            }));

            var result = (ObjectResult)await controller.GetEpicTask(_defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateEpicTaskOk()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.UpdateEpicTask(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteEpicTaskOk()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.DeleteEpicTask(_defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task CreateJobInEpicTaskOk()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.CreateJobInEpicTask(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            }, _defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteJobFromEpicTaskOk()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.DeleteJobFromEpicTask(_defaultJobId, _defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateEpicTaskError()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            epicTaskService.Setup(r => r.CreateEpicTaskAsync(It.IsAny<EpicTask>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateEpicTask(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetEpicTaskListError()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            epicTaskService.Setup(r => r.GetEpicTasksListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetEpicTasksList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetEpicTaskError()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            epicTaskService.Setup(r => r.GetEpicTaskAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetEpicTask(_defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateEpicTaskError()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            epicTaskService.Setup(r => r.UpdateEpicTaskAsync(It.IsAny<EpicTask>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateEpicTask(new EpicTask()
            {
                Id = 1,
                UserId = 1,
                Description = "desc",
                Title = "title"
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteEpicTaskError()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            epicTaskService.Setup(r => r.DeleteEpicTaskAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteEpicTask(_defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task CreateJobInEpicTaskError()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            gatewayService.Setup(r => r.CreateJobInEpicTaskAsync(It.IsAny<Job>(), It.IsAny<int>())).Throws<Exception>();
            var result = (StatusCodeResult)await controller.CreateJobInEpicTask(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            }, _defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteJobFromEpicTaskError()
        {
            var epicTaskService = new Mock<IEpicTaskService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<EpicTaskController>>();
            var controller = new EpicTaskController(logger.Object, epicTaskService.Object, gatewayService.Object);

            gatewayService.Setup(r => r.DeleteJobFromEpicTaskAsync(It.IsAny<int>(), It.IsAny<int>())).Throws<Exception>();
            var result = (StatusCodeResult)await controller.DeleteJobFromEpicTask(_defaultJobId, _defaultEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
