﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ReverseProxy.Controllers;
using DailyPlanDatabase.Models;
using Microsoft.Extensions.Logging;
using DailyPlanService;
using GatewayService;
using JobDatabase.Models;

namespace ReverseProxy.Test
{
    [TestClass]
    public class DailyPlanTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultJobId = 1;
        private const int _defaultDailyPlanId = 1;
        private const int _from = 1;
        private const int _count = 3;

        #region Ok

        [TestMethod]
        public async Task CreateDailyPlanOk()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.CreateDailyPlan(new DailyPlan()
            {
                Id = 1,
                UserId = 1,
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
        }

        [TestMethod]
        public async Task GetDailyPlanListOk()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            dailyPlanService.Setup(r => r.GetDailyPlansListAsync()).Returns(Task.FromResult(new List<DailyPlan>()
            {
                new DailyPlan() { Id = 1, UserId = 1, CurrentDate = DateTime.Now, DayOfWeek = DayOfWeek.Monday},
                new DailyPlan() { Id = 2, UserId = 2, CurrentDate = DateTime.Now, DayOfWeek = DayOfWeek.Tuesday},
                new DailyPlan() { Id = 3, UserId = 3, CurrentDate = DateTime.Now, DayOfWeek = DayOfWeek.Wednesday},
            }));

            var result = (ObjectResult)await controller.GetDailyPlansList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetDailyPlanOk()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            dailyPlanService.Setup(r => r.GetDailyPlanAsync(It.IsAny<int>())).Returns(Task.FromResult(new DailyPlan()
            {
                Id = 1,
                UserId = 1,
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            }));

            var result = (ObjectResult)await controller.GetDailyPlan(_defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateDailyPlanOk()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.UpdateDailyPlan(new DailyPlan()
            {
                Id = 1,
                UserId = 1,
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteDailyPlanOk()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.DeleteDailyPlan(_defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task CreateJobInDailyPlanOk()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.CreateJobInDailyPlan(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            }, _defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteJobFromDailyPlanOk()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            var result = (StatusCodeResult)await controller.DeleteJobFromDailyPlan(_defaultJobId, _defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateDailyPlanError()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            dailyPlanService.Setup(r => r.CreateDailyPlanAsync(It.IsAny<DailyPlan>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateDailyPlan(new DailyPlan()
            {
                Id = 1,
                UserId = 1,
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetDailyPlanListError()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            dailyPlanService.Setup(r => r.GetDailyPlansListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetDailyPlansList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetDailyPlanError()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            dailyPlanService.Setup(r => r.GetDailyPlanAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetDailyPlan(_defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateDailyPlanError()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            dailyPlanService.Setup(r => r.UpdateDailyPlanAsync(It.IsAny<DailyPlan>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateDailyPlan(new DailyPlan()
            {
                Id = 1,
                UserId = 1,
                CurrentDate = DateTime.Now,
                DayOfWeek = DayOfWeek.Monday
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteDailyPlanError()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            dailyPlanService.Setup(r => r.DeleteDailyPlanAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteDailyPlan(_defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task CreateJobInDailyPlanError()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            gatewayService.Setup(r => r.CreateJobInDailyPlanAsync(It.IsAny<Job>(), It.IsAny<int>())).Throws<Exception>();
            var result = (StatusCodeResult)await controller.CreateJobInDailyPlan(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            }, _defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteJobFromDailyPlanError()
        {
            var dailyPlanService = new Mock<IDailyPlanService>();
            var gatewayService = new Mock<IGatewayService>();
            var logger = new Mock<ILogger<DailyPlanController>>();
            var controller = new DailyPlanController(logger.Object, dailyPlanService.Object, gatewayService.Object);

            gatewayService.Setup(r => r.DeleteJobFromDailyPlanAsync(It.IsAny<int>(), It.IsAny<int>())).Throws<Exception>();
            var result = (StatusCodeResult)await controller.DeleteJobFromDailyPlan(_defaultJobId, _defaultDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
