﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ReverseProxy.Controllers;
using EpicTaskDatabase.Models;
using Microsoft.Extensions.Logging;
using EpicTaskService;

namespace ReverseProxy.Test
{
    [TestClass]
    public class JobToEpicTaskTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultJobToEpicTaskId = 1;
        private const int _from = 1;
        private const int _count = 3;

        #region Ok

        [TestMethod]
        public async Task CreateJobToEpicTaskOk()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.CreateJobToEpicTask(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
        }

        [TestMethod]
        public async Task GetJobToEpicTaskListOk()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            service.Setup(r => r.GetJobToEpicTasksListAsync()).Returns(Task.FromResult(new List<JobToEpicTask>()
            {
                new JobToEpicTask() { Id = 1, EpicTaskId = 1, JobId = 1 },
                new JobToEpicTask() { Id = 2, EpicTaskId = 2, JobId = 2 },
                new JobToEpicTask() { Id = 3, EpicTaskId = 3, JobId = 3 },
            }));

            var result = (ObjectResult)await controller.GetJobToEpicTasksList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetJobToEpicTaskOk()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            service.Setup(r => r.GetJobToEpicTaskAsync(It.IsAny<int>())).Returns(Task.FromResult(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            }));

            var result = (ObjectResult)await controller.GetJobToEpicTask(_defaultJobToEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateJobToEpicTaskOk()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.UpdateJobToEpicTask(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteJobToEpicTaskOk()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.DeleteJobToEpicTask(_defaultJobToEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateJobToEpicTaskError()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            service.Setup(r => r.CreateJobToEpicTaskAsync(It.IsAny<JobToEpicTask>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateJobToEpicTask(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetJobToEpicTaskListError()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            service.Setup(r => r.GetJobToEpicTasksListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobToEpicTasksList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetJobToEpicTaskError()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            service.Setup(r => r.GetJobToEpicTaskAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobToEpicTask(_defaultJobToEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateJobToEpicTaskError()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            service.Setup(r => r.UpdateJobToEpicTaskAsync(It.IsAny<JobToEpicTask>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateJobToEpicTask(new JobToEpicTask()
            {
                Id = 1,
                EpicTaskId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteJobToEpicTaskError()
        {
            var service = new Mock<IEpicTaskService>();
            var logger = new Mock<ILogger<JobToEpicTaskController>>();
            var controller = new JobToEpicTaskController(logger.Object, service.Object);

            service.Setup(r => r.DeleteJobToEpicTaskAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteJobToEpicTask(_defaultJobToEpicTaskId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
