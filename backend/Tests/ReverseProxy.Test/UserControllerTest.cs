﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ReverseProxy.Controllers;
using UserDatabase.Models;
using Microsoft.Extensions.Logging;
using UserService;

namespace ReverseProxy.Test
{
    [TestClass]
    public class UserTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultUserId = 1;
        private const int _from = 1;
        private const int _count = 3;

        #region Ok

        [TestMethod]
        public async Task CreateUserOk()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.CreateUser(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
        }

        [TestMethod]
        public async Task GetUserListOk()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            service.Setup(r => r.GetUsersListAsync()).Returns(Task.FromResult(new List<User>()
            {
                new User() { Id = 1, Name = "1", Surname = "1", DateOfBirth = DateTime.Now},
                new User() { Id = 1, Name = "2", Surname = "2", DateOfBirth = DateTime.Now},
                new User() { Id = 1, Name = "3", Surname = "3", DateOfBirth = DateTime.Now},
            }));

            var result = (ObjectResult)await controller.GetUsersList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetUserOk()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            service.Setup(r => r.GetUserAsync(It.IsAny<int>())).Returns(Task.FromResult(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            }));

            var result = (ObjectResult)await controller.GetUser(_defaultUserId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateUserOk()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.UpdateUser(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteUserOk()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.DeleteUser(_defaultUserId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateUserError()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            service.Setup(r => r.CreateUserAsync(It.IsAny<User>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateUser(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetUserListError()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            service.Setup(r => r.GetUsersListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetUsersList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetUserError()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            service.Setup(r => r.GetUserAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetUser(_defaultUserId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateUserError()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            service.Setup(r => r.UpdateUserAsync(It.IsAny<User>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateUser(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                DateOfBirth = DateTime.Now
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteUserError()
        {
            var service = new Mock<IUserService>();
            var logger = new Mock<ILogger<UserController>>();
            var controller = new UserController(logger.Object, service.Object);

            service.Setup(r => r.DeleteUserAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteUser(_defaultUserId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
