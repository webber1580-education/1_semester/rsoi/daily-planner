﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ReverseProxy.Controllers;
using JobDatabase.Models;
using Microsoft.Extensions.Logging;
using JobService;

namespace ReverseProxy.Test
{
    [TestClass]
    public class JobTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultJobId = 1;
        private const int _from = 1;
        private const int _count = 3;

        #region Ok

        [TestMethod]
        public async Task CreateJobOk()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.CreateJob(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
        }

        [TestMethod]
        public async Task GetJobListOk()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            service.Setup(r => r.GetJobsListAsync()).Returns(Task.FromResult(new List<Job>()
            {
                new Job() { Id = 1, PreorityNumber = 1, Description = "1", Title = "1", UserId = 1},
                new Job() { Id = 2, PreorityNumber = 2, Description = "2", Title = "2", UserId = 2},
                new Job() { Id = 3, PreorityNumber = 3, Description = "3", Title = "3", UserId = 3},
            }));

            var result = (ObjectResult)await controller.GetJobsList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetJobOk()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            service.Setup(r => r.GetJobAsync(It.IsAny<int>())).Returns(Task.FromResult(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            }));

            var result = (ObjectResult)await controller.GetJob(_defaultJobId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateJobOk()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.UpdateJob(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteJobOk()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.DeleteJob(_defaultJobId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateJobError()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            service.Setup(r => r.CreateJobAsync(It.IsAny<Job>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateJob(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetJobListError()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            service.Setup(r => r.GetJobsListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobsList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetJobError()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            service.Setup(r => r.GetJobAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJob(_defaultJobId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateJobError()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            service.Setup(r => r.UpdateJobAsync(It.IsAny<Job>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateJob(new Job()
            {
                Id = 1,
                PreorityNumber = 1,
                Description = "descr",
                Title = "title",
                UserId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteJobError()
        {
            var service = new Mock<IJobService>();
            var logger = new Mock<ILogger<JobController>>();
            var controller = new JobController(logger.Object, service.Object);

            service.Setup(r => r.DeleteJobAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteJob(_defaultJobId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
