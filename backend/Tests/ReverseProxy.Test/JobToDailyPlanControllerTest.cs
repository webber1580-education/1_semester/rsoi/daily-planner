﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ReverseProxy.Controllers;
using DailyPlanDatabase.Models;
using Microsoft.Extensions.Logging;
using DailyPlanService;

namespace ReverseProxy.Test
{
    [TestClass]
    public class JobToDailyPlanTests
    {
        private const int _statusCodeOk = 200;
        private const int _statusCodeCreated = 201;
        private const int _statusCodeInternalServerError = 500;
        private const int _statusCodeNotFound = 404;
        private const int _defaultJobToDailyPlanId = 1;
        private const int _from = 1;
        private const int _count = 3;

        #region Ok

        [TestMethod]
        public async Task CreateJobToDailyPlanOk()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.CreateJobToDailyPlan(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeCreated);
        }

        [TestMethod]
        public async Task GetJobToDailyPlanListOk()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            service.Setup(r => r.GetJobToDailyPlansListAsync()).Returns(Task.FromResult(new List<JobToDailyPlan>()
            {
                new JobToDailyPlan() { Id = 1, DailyPlanId = 1, JobId = 1 },
                new JobToDailyPlan() { Id = 2, DailyPlanId = 2, JobId = 2 },
                new JobToDailyPlan() { Id = 3, DailyPlanId = 3, JobId = 3 },
            }));

            var result = (ObjectResult)await controller.GetJobToDailyPlansList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task GetJobToDailyPlanOk()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            service.Setup(r => r.GetJobToDailyPlanAsync(It.IsAny<int>())).Returns(Task.FromResult(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            }));

            var result = (ObjectResult)await controller.GetJobToDailyPlan(_defaultJobToDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task UpdateJobToDailyPlanOk()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.UpdateJobToDailyPlan(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        [TestMethod]
        public async Task DeleteJobToDailyPlanOk()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            var result = (StatusCodeResult)await controller.DeleteJobToDailyPlan(_defaultJobToDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeOk);
        }

        #endregion

        #region Error

        [TestMethod]
        public async Task CreateJobToDailyPlanError()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            service.Setup(r => r.CreateJobToDailyPlanAsync(It.IsAny<JobToDailyPlan>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.CreateJobToDailyPlan(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task GetJobToDailyPlanListError()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            service.Setup(r => r.GetJobToDailyPlansListAsync()).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobToDailyPlansList(_from, _count);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task GetJobToDailyPlanError()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            service.Setup(r => r.GetJobToDailyPlanAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (NotFoundResult)await controller.GetJobToDailyPlan(_defaultJobToDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeNotFound);
        }

        [TestMethod]
        public async Task UpdateJobToDailyPlanError()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            service.Setup(r => r.UpdateJobToDailyPlanAsync(It.IsAny<JobToDailyPlan>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.UpdateJobToDailyPlan(new JobToDailyPlan()
            {
                Id = 1,
                DailyPlanId = 1,
                JobId = 1
            });
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        [TestMethod]
        public async Task DeleteJobToDailyPlanError()
        {
            var service = new Mock<IDailyPlanService>();
            var logger = new Mock<ILogger<JobToDailyPlanController>>();
            var controller = new JobToDailyPlanController(logger.Object, service.Object);

            service.Setup(r => r.DeleteJobToDailyPlanAsync(It.IsAny<int>())).Throws<Exception>();

            var result = (StatusCodeResult)await controller.DeleteJobToDailyPlan(_defaultJobToDailyPlanId);
            Assert.AreEqual(result.StatusCode, _statusCodeInternalServerError);
        }

        #endregion
    }
}
