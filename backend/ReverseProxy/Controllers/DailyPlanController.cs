﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DailyPlanDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using DailyPlanService;
using System.Collections.Generic;
using JobDatabase.Models;
using GatewayService;

namespace ReverseProxy.Controllers
{
    [Route("api/dailyPlan")]
    [ApiController]
    public class DailyPlanController : BaseController<DailyPlanController>
    {
        private readonly IDailyPlanService _dailyPlanService;
        private readonly IGatewayService _gatewayService;

        public DailyPlanController(ILogger<DailyPlanController> logger, 
            IDailyPlanService dailyPlanService,
            IGatewayService gatewayService)
            : base(logger)
        {
            _dailyPlanService = dailyPlanService;
            _gatewayService = gatewayService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateDailyPlan([FromBody] DailyPlan dailyPlan)
        {
            int statusCode;
            try
            {
                var dailyPlanId = await _dailyPlanService.CreateDailyPlanAsync(dailyPlan);
                _logger.LogInformation($"DailyPlan was successfully created with id {dailyPlanId}");
                statusCode = 201;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new dailyPlan: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpGet]
        public async Task<IActionResult> GetDailyPlansList(int from, int count)
        {
            IActionResult response;
            try
            {
                var dailyPlanList = await _dailyPlanService.GetDailyPlansListAsync();

                if (from + count > dailyPlanList.Count || count == 0)
                    count = dailyPlanList.Count - from;

                _logger.LogInformation($"DailyPlanList was successfully received: " +
                    $"{JsonConvert.SerializeObject(dailyPlanList.GetRange(from, count))}");
                response = Ok(dailyPlanList.GetRange(from, count));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of dailyPlans: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetDailyPlan(int id)
        {
            IActionResult response;
            try
            {
                var dailyPlan = await _dailyPlanService.GetDailyPlanAsync(id);
                _logger.LogInformation($"DailyPlan with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(dailyPlan)}");
                response = Ok(dailyPlan);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving dailyPlan with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateDailyPlan([FromBody] DailyPlan dailyPlan)
        {
            int statusCode;
            try
            {
                await _dailyPlanService.UpdateDailyPlanAsync(dailyPlan);
                _logger.LogInformation($"DailyPlan with id {dailyPlan.Id} was successfully updated: " +
                    $"{JsonConvert.SerializeObject(dailyPlan)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating dailyPlan with id {dailyPlan.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDailyPlan(int id)
        {
            int statusCode;
            try
            {
                await _dailyPlanService.DeleteDailyPlanAsync(id);
                _logger.LogInformation($"DailyPlan with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in deleting dailyPlan with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpPost("{dailyPlanId}")]
        public async Task<IActionResult> CreateJobInDailyPlan([FromBody] Job job, int dailyPlanId)
        {
            int statusCode;
            try
            {
                await _gatewayService.CreateJobInDailyPlanAsync(job, dailyPlanId);
                _logger.LogInformation($"Job {JsonConvert.SerializeObject(job)} was successfully created in" +
                    $"epictask with id {dailyPlanId}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new job in dailyPlan: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{jobId}/{dailyPlanId}")]
        public async Task<IActionResult> DeleteJobFromDailyPlan(int jobId, int dailyPlanId)
        {
            int statusCode;
            try
            {
                await _gatewayService.DeleteJobFromDailyPlanAsync(jobId, dailyPlanId);
                _logger.LogInformation($"Job with id {jobId} was successfully removed from dailyPlan with id {dailyPlanId}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in deleting job with id {jobId} form " +
                    $"dailyPlan with id {dailyPlanId}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
