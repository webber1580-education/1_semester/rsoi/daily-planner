﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DailyPlanDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using DailyPlanService;
using System.Collections.Generic;

namespace ReverseProxy.Controllers
{
    [Route("api/jobToDailyPlan")]
    [ApiController]
    public class JobToDailyPlanController : BaseController<JobToDailyPlanController>
    {
        private readonly IDailyPlanService _dailyPlanService;

        public JobToDailyPlanController(ILogger<JobToDailyPlanController> logger, IDailyPlanService dailyPlanService)
            : base(logger)
        {
            _dailyPlanService = dailyPlanService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateJobToDailyPlan([FromBody] JobToDailyPlan jobToDailyPlan)
        {
            int statusCode;
            try
            {
                var jobToDailyPlanId = await _dailyPlanService.CreateJobToDailyPlanAsync(jobToDailyPlan);
                _logger.LogInformation($"JobToDailyPlan was successfully created with id {jobToDailyPlanId}");
                statusCode = 201;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new jobToDailyPlan: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpGet]
        public async Task<IActionResult> GetJobToDailyPlansList(int from, int count)
        {
            IActionResult response;
            try
            {
                var jobToDailyPlanList = await _dailyPlanService.GetJobToDailyPlansListAsync();

                if (from + count > jobToDailyPlanList.Count || count == 0)
                    count = jobToDailyPlanList.Count - from;

                _logger.LogInformation($"JobToDailyPlanList was successfully received: " +
                    $"{JsonConvert.SerializeObject(jobToDailyPlanList.GetRange(from, count))}");
                response = Ok(jobToDailyPlanList.GetRange(from, count));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of jobToDailyPlans: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetJobToDailyPlan(int id)
        {
            IActionResult response;
            try
            {
                var jobToDailyPlan = await _dailyPlanService.GetJobToDailyPlanAsync(id);
                _logger.LogInformation($"JobToDailyPlan with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(jobToDailyPlan)}");
                response = Ok(jobToDailyPlan);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving jobToDailyPlan with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateJobToDailyPlan([FromBody] JobToDailyPlan jobToDailyPlan)
        {
            int statusCode;
            try
            {
                await _dailyPlanService.UpdateJobToDailyPlanAsync(jobToDailyPlan);
                _logger.LogInformation($"JobToDailyPlan with id {jobToDailyPlan.Id} was successfully updated: " +
                    $"{JsonConvert.SerializeObject(jobToDailyPlan)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating jobToDailyPlan with id {jobToDailyPlan.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteJobToDailyPlan(int id)
        {
            int statusCode;
            try
            {
                await _dailyPlanService.DeleteJobToDailyPlanAsync(id);
                _logger.LogInformation($"JobToDailyPlan with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in deleting jobToDailyPlan with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
