﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using UserService;
using System.Collections.Generic;

namespace ReverseProxy.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : BaseController<UserController>
    {
        private readonly IUserService _userService;

        public UserController(ILogger<UserController> logger, IUserService userService)
            : base(logger)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] User user)
        {
            int statusCode;
            try 
            {
                var userId = await _userService.CreateUserAsync(user);
                _logger.LogInformation($"User was successfully created with id {userId}");
                statusCode = 201;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new user: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpGet]
        public async Task<IActionResult> GetUsersList([FromQuery] int from, [FromQuery] int count)
        {
            IActionResult response;
            try
            {
                var userList = await _userService.GetUsersListAsync();

                if (from + count > userList.Count || count == 0)
                    count = userList.Count - from;

                _logger.LogInformation($"UserList was successfully received: " +
                    $"{JsonConvert.SerializeObject(userList.GetRange(from, count))}");
                response = Ok(userList.GetRange(from, count));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of users: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            IActionResult response;
            try
            {
                var user = await _userService.GetUserAsync(id);
                _logger.LogInformation($"User with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(user)}");
                response = Ok(user);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving user with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser([FromBody] User user)
        {
            int statusCode;
            try
            {
                await _userService.UpdateUserAsync(user);
                _logger.LogInformation($"User with id {user.Id} was successfully updated: " +
                    $"{JsonConvert.SerializeObject(user)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating user with id {user.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            int statusCode;
            try
            {
                await _userService.DeleteUserAsync(id);
                _logger.LogInformation($"User with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in deleting user with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
