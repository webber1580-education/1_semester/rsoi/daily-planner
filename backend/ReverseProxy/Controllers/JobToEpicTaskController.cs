﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EpicTaskDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using EpicTaskService;
using System.Collections.Generic;

namespace ReverseProxy.Controllers
{
    [Route("api/jobToEpicTask")]
    [ApiController]
    public class JobToEpicTaskController : BaseController<JobToEpicTaskController>
    {
        private readonly IEpicTaskService _epicTaskService;

        public JobToEpicTaskController(ILogger<JobToEpicTaskController> logger, IEpicTaskService epicTaskService)
            : base(logger)
        {
            _epicTaskService = epicTaskService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateJobToEpicTask([FromBody] JobToEpicTask jobToEpicTask)
        {
            int statusCode;
            try
            {
                var jobToEpicTaskId = await _epicTaskService.CreateJobToEpicTaskAsync(jobToEpicTask);
                _logger.LogInformation($"JobToEpicTask was successfully created with id {jobToEpicTaskId}");
                statusCode = 201;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new jobToEpicTask: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpGet]
        public async Task<IActionResult> GetJobToEpicTasksList(int from, int count)
        {
            IActionResult response;
            try
            {
                var jobToEpicTaskList = await _epicTaskService.GetJobToEpicTasksListAsync();

                if (from + count > jobToEpicTaskList.Count || count == 0)
                    count = jobToEpicTaskList.Count - from;

                _logger.LogInformation($"JobToEpicTaskList was successfully received: " +
                    $"{JsonConvert.SerializeObject(jobToEpicTaskList.GetRange(from, count))}");
                response = Ok(jobToEpicTaskList.GetRange(from, count));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of jobToEpicTasks: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetJobToEpicTask(int id)
        {
            IActionResult response;
            try
            {
                var jobToEpicTask = await _epicTaskService.GetJobToEpicTaskAsync(id);
                _logger.LogInformation($"JobToEpicTask with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(jobToEpicTask)}");
                response = Ok(jobToEpicTask);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving jobToEpicTask with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateJobToEpicTask([FromBody] JobToEpicTask jobToEpicTask)
        {
            int statusCode;
            try
            {
                await _epicTaskService.UpdateJobToEpicTaskAsync(jobToEpicTask);
                _logger.LogInformation($"JobToEpicTask with id {jobToEpicTask.Id} was successfully updated: " +
                    $"{JsonConvert.SerializeObject(jobToEpicTask)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating jobToEpicTask with id {jobToEpicTask.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteJobToEpicTask(int id)
        {
            int statusCode;
            try
            {
                await _epicTaskService.DeleteJobToEpicTaskAsync(id);
                _logger.LogInformation($"JobToEpicTask with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in deleting jobToEpicTask with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
