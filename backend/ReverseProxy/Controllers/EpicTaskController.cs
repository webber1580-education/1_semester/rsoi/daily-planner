﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EpicTaskDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using EpicTaskService;
using System.Collections.Generic;
using JobDatabase.Models;
using GatewayService;

namespace ReverseProxy.Controllers
{
    [Route("api/epicTask")]
    [ApiController]
    public class EpicTaskController : BaseController<EpicTaskController>
    {
        private readonly IEpicTaskService _epicTaskService;
        private readonly IGatewayService _gatewayService;

        public EpicTaskController(ILogger<EpicTaskController> logger, 
            IEpicTaskService epicTaskService, 
            IGatewayService gatewayService)
            : base(logger)
        {
            _epicTaskService = epicTaskService;
            _gatewayService = gatewayService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateEpicTask([FromBody] EpicTask epicTask)
        {
            int statusCode;
            try
            {
                var epicTaskId = await _epicTaskService.CreateEpicTaskAsync(epicTask);
                _logger.LogInformation($"EpicTask was successfully created with id {epicTaskId}");
                statusCode = 201;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new epicTask: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpGet]
        public async Task<IActionResult> GetEpicTasksList(int from, int count)
        {
            IActionResult response;
            try
            {
                var epicTaskList = await _epicTaskService.GetEpicTasksListAsync();

                if (from + count > epicTaskList.Count || count == 0)
                    count = epicTaskList.Count - from;

                _logger.LogInformation($"EpicTaskList was successfully received: " +
                    $"{JsonConvert.SerializeObject(epicTaskList.GetRange(from, count))}");
                response = Ok(epicTaskList.GetRange(from, count));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of epicTasks: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEpicTask(int id)
        {
            IActionResult response;
            try
            {
                var epicTask = await _epicTaskService.GetEpicTaskAsync(id);
                _logger.LogInformation($"EpicTask with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(epicTask)}");
                response = Ok(epicTask);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving epicTask with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateEpicTask([FromBody] EpicTask epicTask)
        {
            int statusCode;
            try
            {
                await _epicTaskService.UpdateEpicTaskAsync(epicTask);
                _logger.LogInformation($"EpicTask with id {epicTask.Id} was successfully updated: " +
                    $"{JsonConvert.SerializeObject(epicTask)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating epicTask with id {epicTask.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEpicTask(int id)
        {
            int statusCode;
            try
            {
                await _epicTaskService.DeleteEpicTaskAsync(id);
                _logger.LogInformation($"EpicTask with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in deleting epicTask with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpPost("{epicTaskId}")]
        public async Task<IActionResult> CreateJobInEpicTask(Job job, int epicTaskId)
        {
            int statusCode;
            try
            {
                await _gatewayService.CreateJobInEpicTaskAsync(job, epicTaskId);
                _logger.LogInformation($"Job {JsonConvert.SerializeObject(job)} was successfully created in" +
                    $"epictask with id {epicTaskId}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new job in epicTask: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{jobId}/{epicTaskId}")]
        public async Task<IActionResult> DeleteJobFromEpicTask(int jobId, int epicTaskId)
        {
            int statusCode;
            try
            {
                await _gatewayService.DeleteJobFromEpicTaskAsync(jobId, epicTaskId);
                _logger.LogInformation($"Job with id {jobId} was successfully removed from epicTask with id {epicTaskId}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in deleting job with id {jobId} form " +
                    $"epicTask with id {epicTaskId}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }
    }
}
