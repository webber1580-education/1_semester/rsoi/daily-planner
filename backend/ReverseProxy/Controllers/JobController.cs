﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JobDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using JobService;
using System.Collections.Generic;

namespace ReverseProxy.Controllers
{
    [Route("api/job")]
    [ApiController]
    public class JobController : BaseController<JobController>
    {
        private readonly IJobService _jobService;

        public JobController(ILogger<JobController> logger, IJobService jobService)
            : base(logger)
        {
            _jobService = jobService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateJob([FromBody] Job job)
        {
            int statusCode;
            try
            {
                var jobId = await _jobService.CreateJobAsync(job);
                _logger.LogInformation($"Job was successfully created with id {jobId}");
                statusCode = 201;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new job: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpGet]
        public async Task<IActionResult> GetJobsList(int from, int count)
        {
            IActionResult response;
            try
            {
                var jobList = await _jobService.GetJobsListAsync();

                if (from + count > jobList.Count || count == 0)
                    count = jobList.Count - from;

                _logger.LogInformation($"JobList was successfully received: " +
                    $"{JsonConvert.SerializeObject(jobList.GetRange(from, count))}");
                response = Ok(jobList.GetRange(from, count));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of jobs: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetJob(int id)
        {
            IActionResult response;
            try
            {
                var job = await _jobService.GetJobAsync(id);
                _logger.LogInformation($"Job with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(job)}");
                response = Ok(job);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving job with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateJob([FromBody] Job job)
        {
            int statusCode;
            try
            {
                await _jobService.UpdateJobAsync(job);
                _logger.LogInformation($"Job with id {job.Id} was successfully updated: " +
                    $"{JsonConvert.SerializeObject(job)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating job with id {job.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteJob(int id)
        {
            int statusCode;
            try
            {
                await _jobService.DeleteJobAsync(id);
                _logger.LogInformation($"Job with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in deleting job with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
