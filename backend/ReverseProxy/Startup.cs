﻿using DailyPlanHttpService;
using DailyPlanService;
using EpicTaskHttpService;
using EpicTaskService;
using GatewayService;
using JobHttpService;
using JobService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using UserHttpService;
using UserService;

namespace ReverseProxy
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowCredentials()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ReverseProxy API", Version = "v1" });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDailyPlanHttpService(Configuration.GetSection("DailyPlanWebService"));
            services.AddEpicTaskHttpService(Configuration.GetSection("EpicTaskWebService"));
            services.AddJobHttpService(Configuration.GetSection("JobWebService"));
            services.AddUserHttpService(Configuration.GetSection("UserWebService"));
            services.AddGatewayService();
            services.AddDailyPlanService();
            services.AddEpicTaskService();
            services.AddJobService();
            services.AddUserService();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ReverseProxy API");
            });

            app.UseMvc();
        }
    }

}
