﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HttpRequest
{
    public class PostRequest
    {
        public static async Task<HttpResponseMessage> ExecuteAsync(string url, IEnumerable<KeyValuePair<string, string>> postRequestBody)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, url)
            {
                Content = new FormUrlEncodedContent(postRequestBody)
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (Exception e)
            {
                response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.GatewayTimeout
                };
            }

            return response;
        }

        public static async Task<HttpResponseMessage> ExecuteAsync(string url, string postJsonRequestBody)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, url)
            {
                Content = new StringContent(postJsonRequestBody, Encoding.UTF8, "application/json")
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (Exception e)
            {
                response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.GatewayTimeout
                };
            }

            return response;
        }
    }
}
