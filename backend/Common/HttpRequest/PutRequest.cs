﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HttpRequest
{
    public class PutRequest
    {
        public static async Task<HttpResponseMessage> ExecuteAsync(string url, IEnumerable<KeyValuePair<string, string>> putRequestBody)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Put, url)
            {
                Content = new FormUrlEncodedContent(putRequestBody)
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (Exception e)
            {
                response = new HttpResponseMessage();
                response.Content = new StringContent(e.Message);
            }

            return response;
        }

        public static async Task<HttpResponseMessage> ExecuteAsync(string url, string putJsonRequestBody)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Put, url)
            {
                Content = new StringContent(putJsonRequestBody, Encoding.UTF8, "application/json")
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (Exception e)
            {
                response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.GatewayTimeout
                };
            }

            return response;
        }

        public static async Task<HttpResponseMessage> ExecuteAsync(string url)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Put, url);

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (Exception e)
            {
                response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.GatewayTimeout
                };
            }

            return response;
        }

    }
}
