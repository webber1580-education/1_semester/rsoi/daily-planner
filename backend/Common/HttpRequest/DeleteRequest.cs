﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HttpRequest
{
    public class DeleteRequest
    {
        public static async Task<HttpResponseMessage> ExecuteAsync(string url)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Delete, url);

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (Exception e)
            {
                response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.GatewayTimeout
                };
            }

            return response;
        }
    }
}
