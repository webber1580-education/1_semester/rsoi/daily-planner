﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.ExceptionHandler
{
    public class OperationExecutingResult
    {
        public bool Success { get; }

        public Exception Exception { get; }

        public OperationExecutingResult()
        {
            Success = true;
            Exception = null;
        }

        public OperationExecutingResult(Exception exception)
        {
            Success = false;
            Exception = exception;
        }
    }

    public class OperationExecutingResult<T>
    {
        public bool Success { get; }

        public Exception Exception { get; }

        public T Result { get; }

        public OperationExecutingResult(T result)
        {
            Success = true;
            Exception = null;
            Result = result;
        }

        public OperationExecutingResult(Exception exception)
        {
            Success = false;
            Exception = exception;
            Result = default(T);
        }
    }
}
