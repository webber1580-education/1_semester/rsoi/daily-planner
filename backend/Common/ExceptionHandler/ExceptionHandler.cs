﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.ExceptionHandler
{
    public class ExceptionHandler
    {
        private static OperationExecutingResult ExecuteOperation(Action operation)
        {
            OperationExecutingResult operationResult = null;

            if (operation == null)
                operationResult = new OperationExecutingResult(new ArgumentNullException(nameof(operation)));

            if (operationResult == null)
            {
                try
                {
                    operation.Invoke();
                    operationResult = new OperationExecutingResult();
                }
                catch (Exception e)
                {
                    operationResult = new OperationExecutingResult(e);
                }
            }

            return operationResult;
        }

        private static async Task<OperationExecutingResult> ExecuteOperationAsync(Func<Task> operation)
        {
            OperationExecutingResult operationResult = null;

            if (operation == null)
                operationResult = new OperationExecutingResult(new ArgumentNullException(nameof(operation)));

            if (operationResult == null)
            {
                try
                {
                    await operation.Invoke();
                    operationResult = new OperationExecutingResult();
                }
                catch (Exception e)
                {
                    operationResult = new OperationExecutingResult(e);
                }
            }

            return operationResult;
        }

        private static OperationExecutingResult<T> ExecuteOperation<T>(Func<T> operation)
        {
            OperationExecutingResult<T> operationResult = null;

            if (operation == null)
                operationResult = new OperationExecutingResult<T>(new ArgumentNullException(nameof(operation)));

            T result;
            if (operationResult == null)
            {
                try
                {
                    result = operation.Invoke();
                    operationResult = new OperationExecutingResult<T>(result);
                }
                catch (Exception e)
                {
                    operationResult = new OperationExecutingResult<T>(e);
                }
            }

            return operationResult;
        }

        private static async Task<OperationExecutingResult<T>> ExecuteOperationAsync<T>(Func<Task<T>> operation)
        {
            OperationExecutingResult<T> operationResult = null;

            if (operation == null)
                operationResult = new OperationExecutingResult<T>(new ArgumentNullException(nameof(operation)));

            T result;
            if (operationResult == null)
            {
                try
                {
                    result = await operation.Invoke();
                    operationResult = new OperationExecutingResult<T>(result);
                }
                catch (Exception e)
                {
                    operationResult = new OperationExecutingResult<T>(e);
                }
            }

            return operationResult;
        }

        public static async Task RollBackAndThrowAsync(IEnumerable<Func<Task>> rollbackOperations, Exception restreamerEx)
        {
            var exceptions = await RollBackAsync(rollbackOperations);
            exceptions.AddFirst(restreamerEx);
            throw new AggregateException(exceptions);
        }


        public static async Task<LinkedList<Exception>> RollBackAsync(IEnumerable<Func<Task>> rollbackOperations)
        {
            var exceptions = new LinkedList<Exception>();
            foreach (var operation in rollbackOperations)
            {
                var operationResult = await ExecuteOperationAsync(operation);
                if (!operationResult.Success)
                    exceptions.AddLast(operationResult.Exception);
            }

            return exceptions;
        }
    }
}
