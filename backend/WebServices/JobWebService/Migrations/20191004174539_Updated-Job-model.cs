﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JobWebService.Migrations
{
    public partial class UpdatedJobmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                schema: "job-db",
                table: "Jobs",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                schema: "job-db",
                table: "Jobs");
        }
    }
}
