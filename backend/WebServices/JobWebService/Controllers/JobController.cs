﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JobDatabase.Repository;
using JobDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace JobWebService.Controllers
{
    [Route("api/job")]
    [ApiController]
    public class JobController : BaseController<JobController>
    {
        private readonly IJobRepository _jobRepository;

        public JobController(ILogger<JobController> logger, IJobRepository jobRepository)
            : base(logger)
        {
            _jobRepository = jobRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateJob([FromBody] Job job)
        {
            IActionResult response;
            try
            {
                await _jobRepository.CreateJobAsync(job);
                _logger.LogInformation($"Job was successfully created with id {job.Id}");
                response = StatusCode(201, job.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new job: {ex.Message}");
                response = StatusCode(500);
            }

            return response;
        }

        [HttpGet]
        public async Task<IActionResult> GetJobsList()
        {
            IActionResult response;
            try
            {
                var jobList = await _jobRepository.GetJobsListAsync();
                _logger.LogInformation($"JobList was successfully received: {JsonConvert.SerializeObject(jobList)}");
                response = Ok(jobList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of jobs: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetJob(int id)
        {
            IActionResult response;
            try
            {
                var job = await _jobRepository.GetJobAsync(id);
                _logger.LogInformation($"Job with id {id} was successfully received: {JsonConvert.SerializeObject(job)}");
                response = Ok(job);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving job with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateJob([FromBody] Job job)
        {
            int statusCode;
            try
            {
                await _jobRepository.UpdateJobAsync(job);
                _logger.LogInformation($"Job with id {job.Id} was successfully updated: {JsonConvert.SerializeObject(job)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating job with id {job.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteJob(int id)
        {
            int statusCode;
            try
            {
                await _jobRepository.DeleteJobAsync(id);
                _logger.LogInformation($"Job with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating job with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
