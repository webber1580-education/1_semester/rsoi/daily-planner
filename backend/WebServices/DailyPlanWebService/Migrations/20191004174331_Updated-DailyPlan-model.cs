﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DailyPlanWebService.Migrations
{
    public partial class UpdatedDailyPlanmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TaskToDailyPlans",
                schema: "dailyPlan-db");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                schema: "dailyPlan-db",
                table: "DailyPlans",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "JobToDailyPlans",
                schema: "dailyPlan-db",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    JobId = table.Column<int>(nullable: false),
                    DailyPlanId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobToDailyPlans", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobToDailyPlans",
                schema: "dailyPlan-db");

            migrationBuilder.DropColumn(
                name: "UserId",
                schema: "dailyPlan-db",
                table: "DailyPlans");

            migrationBuilder.CreateTable(
                name: "TaskToDailyPlans",
                schema: "dailyPlan-db",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DailyPlanId = table.Column<int>(nullable: false),
                    TaskId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskToDailyPlans", x => x.Id);
                });
        }
    }
}
