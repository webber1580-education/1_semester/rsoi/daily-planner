﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DailyPlanDatabase.Repository;
using DailyPlanDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace JobToDailyPlanWebService.Controllers
{
    [Route("api/jobtodailyplan")]
    [ApiController]
    public class JobToDailyPlanController : BaseController<JobToDailyPlanController>
    {
        private readonly IJobToDailyPlanRepository _jobToDailyPlanRepository;

        public JobToDailyPlanController(ILogger<JobToDailyPlanController> logger, 
            IJobToDailyPlanRepository jobToDailyPlanRepository)
            : base(logger)
        {
            _jobToDailyPlanRepository = jobToDailyPlanRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateJobToDailyPlan([FromBody] JobToDailyPlan jobToDailyPlan)
        {
            IActionResult response;
            try
            {
                await _jobToDailyPlanRepository.CreateJobToDailyPlanAsync(jobToDailyPlan);
                _logger.LogInformation($"JobToDailyPlan was successfully created with id {jobToDailyPlan.Id}");
                response = StatusCode(201, jobToDailyPlan.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new jobToDailyPlan: {ex.Message}");
                response = StatusCode(500);
            }

            return response;
        }

        [HttpGet]
        public async Task<IActionResult> GetJobToDailyPlansList()
        {
            IActionResult response;
            try
            {
                var jobToDailyPlanList = await _jobToDailyPlanRepository.GetJobToDailyPlansListAsync();
                _logger.LogInformation($"JobToDailyPlanList was successfully received:" +
                    $" {JsonConvert.SerializeObject(jobToDailyPlanList)}");
                response = Ok(jobToDailyPlanList);
            }
            catch (Exception)
            {
                _logger.LogError("Error has ocured in recieving list of jobToDailyPlans");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetJobToDailyPlan(int id)
        {
            IActionResult response;
            try
            {
                var jobToDailyPlan = await _jobToDailyPlanRepository.GetJobToDailyPlanAsync(id);
                _logger.LogInformation($"JobToDailyPlan with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(jobToDailyPlan)}");
                response = Ok(jobToDailyPlan);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving jobToDailyPlan with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateJobToDailyPlan([FromBody] JobToDailyPlan jobToDailyPlan)
        {
            int statusCode;
            try
            {
                await _jobToDailyPlanRepository.UpdateJobToDailyPlanAsync(jobToDailyPlan);
                _logger.LogInformation($"JobToDailyPlan with id {jobToDailyPlan.Id} was successfully updated:" +
                    $" {JsonConvert.SerializeObject(jobToDailyPlan)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating jobToDailyPlan with id {jobToDailyPlan.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteJobToDailyPlan(int id)
        {
            int statusCode;
            try
            {
                await _jobToDailyPlanRepository.DeleteJobToDailyPlanAsync(id);
                _logger.LogInformation($"JobToDailyPlan with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating jobToDailyPlan with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
