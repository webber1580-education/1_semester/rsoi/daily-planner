﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DailyPlanDatabase.Repository;
using DailyPlanDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DailyPlanWebService.Controllers
{
    [Route("api/dailyplan")]
    [ApiController]
    public class DailyPlanController : BaseController<DailyPlanController>
    {
        private readonly IDailyPlanRepository _dailyPlanRepository;

        public DailyPlanController(ILogger<DailyPlanController> logger, IDailyPlanRepository dailyPlanRepository)
            : base(logger)
        {
            _dailyPlanRepository = dailyPlanRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateDailyPlan([FromBody] DailyPlan dailyPlan)
        {
            IActionResult response;
            try
            {
                await _dailyPlanRepository.CreateDailyPlanAsync(dailyPlan);
                _logger.LogInformation($"DailyPlan was successfully created with id {dailyPlan.Id}");
                response = StatusCode(201, dailyPlan.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new dailyPlan: {ex.Message}");
                response = StatusCode(500);
            }

            return response;
        }

        [HttpGet]
        public async Task<IActionResult> GetDailyPlansList()
        {
            IActionResult response;
            try
            {
                var dailyPlanList = await _dailyPlanRepository.GetDailyPlansListAsync();
                _logger.LogInformation($"DailyPlanList was successfully received: {JsonConvert.SerializeObject(dailyPlanList)}");
                response = Ok(dailyPlanList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of dailyPlans: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetDailyPlan(int id)
        {
            IActionResult response;
            try
            {
                var dailyPlan = await _dailyPlanRepository.GetDailyPlanAsync(id);
                _logger.LogInformation($"DailyPlan with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(dailyPlan)}");
                response = Ok(dailyPlan);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving dailyPlan with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateDailyPlan([FromBody] DailyPlan dailyPlan)
        {
            int statusCode;
            try
            {
                await _dailyPlanRepository.UpdateDailyPlanAsync(dailyPlan);
                _logger.LogInformation($"DailyPlan with id {dailyPlan.Id} was successfully updated: " +
                    $"{JsonConvert.SerializeObject(dailyPlan)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating dailyPlan with id {dailyPlan.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDailyPlan(int id)
        {
            int statusCode;
            try
            {
                await _dailyPlanRepository.DeleteDailyPlanAsync(id);
                _logger.LogInformation($"DailyPlan with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating dailyPlan with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
