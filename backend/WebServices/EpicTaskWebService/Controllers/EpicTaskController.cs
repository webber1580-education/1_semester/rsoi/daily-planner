﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EpicTaskDatabase.Repository;
using EpicTaskDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EpicTaskWebService.Controllers
{
    [Route("api/epictask")]
    [ApiController]
    public class EpicTaskController : BaseController<EpicTaskController>
    {
        private readonly IEpicTaskRepository _epicTaskRepository;

        public EpicTaskController(ILogger<EpicTaskController> logger, IEpicTaskRepository epicTaskRepository)
            : base(logger)
        {
            _epicTaskRepository = epicTaskRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateEpicTask([FromBody] EpicTask epicTask)
        {
            IActionResult response;
            try
            {
                await _epicTaskRepository.CreateEpicTaskAsync(epicTask);
                _logger.LogInformation($"EpicTask was successfully created with id {epicTask.Id}");
                response = StatusCode(201, epicTask.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new epicTask: {ex.Message}");
                response = StatusCode(500);
            }

            return response;
        }

        [HttpGet]
        public async Task<IActionResult> GetEpicTasksList()
        {
            IActionResult response;
            try
            {
                var epicTaskList = await _epicTaskRepository.GetEpicTasksListAsync();
                _logger.LogInformation($"EpicTaskList was successfully received: " +
                    $"{JsonConvert.SerializeObject(epicTaskList)}");
                response = Ok(epicTaskList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of epicTasks: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEpicTask(int id)
        {
            IActionResult response;
            try
            {
                var epicTask = await _epicTaskRepository.GetEpicTaskAsync(id);
                _logger.LogInformation($"EpicTask with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(epicTask)}");
                response = Ok(epicTask);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving epicTask with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateEpicTask([FromBody] EpicTask epicTask)
        {
            int statusCode;
            try
            {
                await _epicTaskRepository.UpdateEpicTaskAsync(epicTask);
                _logger.LogInformation($"EpicTask with id {epicTask.Id} was successfully updated: " +
                    $"{JsonConvert.SerializeObject(epicTask)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating epicTask with id {epicTask.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEpicTask(int id)
        {
            int statusCode;
            try
            {
                await _epicTaskRepository.DeleteEpicTaskAsync(id);
                _logger.LogInformation($"EpicTask with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating epicTask with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
