﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EpicTaskDatabase.Repository;
using EpicTaskDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace JobToEpicTaskWebService.Controllers
{
    [Route("api/jobtoepictask")]
    [ApiController]
    public class JobToEpicTaskController : BaseController<JobToEpicTaskController>
    {
        private readonly IJobToEpicTaskRepository _jobToEpicTaskRepository;

        public JobToEpicTaskController(ILogger<JobToEpicTaskController> logger, IJobToEpicTaskRepository jobToEpicTaskRepository)
            : base(logger)
        {
            _jobToEpicTaskRepository = jobToEpicTaskRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateJobToEpicTask([FromBody] JobToEpicTask jobToEpicTask)
        {
            IActionResult response;
            try
            {
                await _jobToEpicTaskRepository.CreateJobToEpicTaskAsync(jobToEpicTask);
                _logger.LogInformation($"JobToEpicTask was successfully created with id {jobToEpicTask.Id}");
                response = StatusCode(201, jobToEpicTask.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new jobToEpicTask: {ex.Message}");
                response = StatusCode(500);
            }

            return response;
        }

        [HttpGet]
        public async Task<IActionResult> GetJobToEpicTasksList()
        {
            IActionResult response;
            try
            {
                var jobToEpicTaskList = await _jobToEpicTaskRepository.GetJobToEpicTasksListAsync();
                _logger.LogInformation($"JobToEpicTaskList was successfully received: " +
                    $"{JsonConvert.SerializeObject(jobToEpicTaskList)}");
                response = Ok(jobToEpicTaskList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of jobToEpicTasks: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetJobToEpicTask(int id)
        {
            IActionResult response;
            try
            {
                var jobToEpicTask = await _jobToEpicTaskRepository.GetJobToEpicTaskAsync(id);
                _logger.LogInformation($"JobToEpicTask with id {id} was successfully received: " +
                    $"{JsonConvert.SerializeObject(jobToEpicTask)}");
                response = Ok(jobToEpicTask);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving jobToEpicTask with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateJobToEpicTask([FromBody] JobToEpicTask jobToEpicTask)
        {
            int statusCode;
            try
            {
                await _jobToEpicTaskRepository.UpdateJobToEpicTaskAsync(jobToEpicTask);
                _logger.LogInformation($"JobToEpicTask with id {jobToEpicTask.Id} was successfully updated: " +
                    $"{JsonConvert.SerializeObject(jobToEpicTask)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating jobToEpicTask with id {jobToEpicTask.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteJobToEpicTask(int id)
        {
            int statusCode;
            try
            {
                await _jobToEpicTaskRepository.DeleteJobToEpicTaskAsync(id);
                _logger.LogInformation($"JobToEpicTask with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating jobToEpicTask with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
