﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EpicTaskWebService.Migrations
{
    public partial class Renameddbschema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "epicTask-db");

            migrationBuilder.RenameTable(
                name: "JobToEpicTasks",
                schema: "dailyPlan-db",
                newName: "JobToEpicTasks",
                newSchema: "epicTask-db");

            migrationBuilder.RenameTable(
                name: "EpicTasks",
                schema: "dailyPlan-db",
                newName: "EpicTasks",
                newSchema: "epicTask-db");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dailyPlan-db");

            migrationBuilder.RenameTable(
                name: "JobToEpicTasks",
                schema: "epicTask-db",
                newName: "JobToEpicTasks",
                newSchema: "dailyPlan-db");

            migrationBuilder.RenameTable(
                name: "EpicTasks",
                schema: "epicTask-db",
                newName: "EpicTasks",
                newSchema: "dailyPlan-db");
        }
    }
}
