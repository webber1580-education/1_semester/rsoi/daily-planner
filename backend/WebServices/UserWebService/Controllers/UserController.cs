﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserDatabase.Repository;
using UserDatabase.Models;
using BaseController;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net;

namespace UserWebService.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : BaseController<UserController>
    {
        private readonly IUserRepository _userRepository;

        public UserController(ILogger<UserController> logger, IUserRepository userRepository) 
            : base(logger)
        {
            _userRepository = userRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] User user)
        {
            IActionResult response;
            try
            {
                await _userRepository.CreateUserAsync(user);
                _logger.LogInformation($"User was successfully created with id {user.Id}");
                response = StatusCode(201, user.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in creating new user: {ex.Message}");
                response = StatusCode(500);
            }

            return response;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsersList()
        {
            IActionResult response;
            try
            {
                var userList = await _userRepository.GetUsersListAsync();
                _logger.LogInformation($"UserList was successfully received: {JsonConvert.SerializeObject(userList)}");
                response = Ok(userList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving list of users: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            IActionResult response;
            try
            {
                var user = await _userRepository.GetUserAsync(id);
                _logger.LogInformation($"User with id {id} was successfully received: {JsonConvert.SerializeObject(user)}");
                response = Ok(user);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in recieving user with id {id}: {ex.Message}");
                response = new NotFoundResult();
            }

            return response;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser([FromBody] User user)
        {
            int statusCode;
            try
            {
                await _userRepository.UpdateUserAsync(user);
                _logger.LogInformation($"User with id {user.Id} was successfully updated: {JsonConvert.SerializeObject(user)}");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating user with id {user.Id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            int statusCode;
            try
            {
                await _userRepository.DeleteUserAsync(id);
                _logger.LogInformation($"User with id {id} was successfully deleted");
                statusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error has ocured in updating user with id {id}: {ex.Message}");
                statusCode = 500;
            }

            return StatusCode(statusCode);
        }

    }
}
