export class Job {
    constructor(
        public id?: number,
        public title?: string,
        public preorityNumber?: number,
        public description?: string,
        public userId?: number) {
            this.preorityNumber = 1;
        }
}
