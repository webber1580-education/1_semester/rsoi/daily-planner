export class JobToDailyPlan {
    constructor(
        public id?: number,
        public jobId?: number,
        public dailyPlanId?: number) {}
}
