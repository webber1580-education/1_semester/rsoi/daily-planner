export class DailyPlan {
    constructor(
        public id?: number,
        public dayOfWeek?: number,
        public currentDate?: any,
        public userId?: number) {}
}

