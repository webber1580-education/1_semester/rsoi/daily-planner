export class JobToEpicTask {
    constructor(
        public id?: number,
        public jobId?: number,
        public epicTaskId?: number) {}
}
