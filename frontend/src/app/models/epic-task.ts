export class EpicTask {
    constructor(
        public id?: number,
        public title?: string,
        public description?: string,
        public userId?: number) {}
}

