import { Component, OnInit } from '@angular/core';
import { JobService } from '../../services/job.service';
import { Job } from '../../models/job';
import { DatePipe } from '@angular/common';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { parse } from 'querystring';
import { tap, map } from 'rxjs/operators';
import { from } from 'rxjs';
import { DailyPlanService } from 'src/app/services/daily-plan.service';
import { EpicTaskService } from 'src/app/services/epic-task.service';

export enum LinkWithEntity {
    nothing = '1',
    dailyPlan = '2',
    epicTask = '3'
}

@Component({
    templateUrl: 'job.component.html',
    providers: [JobService, DailyPlanService, EpicTaskService, DatePipe]
})
export class JobComponent implements OnInit {

    private internalServerErrorStatusCode = 500;

    public pageSize: number;
    public page: number;
    public collectionSize: number;
    public jobCount: number;
    public linkWithEntity: LinkWithEntity;
    public linkEntityId: number;
    public job: Job = new Job();
    public previousJob: Job = new Job();
    public jobs: Job[];
    public tableMode = true;

    constructor(private jobService: JobService,
                private dailyPlanService: DailyPlanService,
                private epicTaskService: EpicTaskService,
                private datePipe: DatePipe) {
        this.page = 1;
        this.pageSize = 5;
    }

    // ----------------------- http requests --------------------- //

    ngOnInit() {
        this.loadJobs();
    }

    public loadJobs() {
        this.getJobsSize();
        this.jobService.getJobs(this.page, this.pageSize).subscribe((data: Job[]) => {
            this.jobs = data;
        });
    }

    public createJob() {
        this.jobService.createJob(this.job).subscribe((data: HttpResponse<Job>) => {
            this.loadJobs();
            this.cancelAddJob();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся добавить задачу. Нет пользователя с заданным идентификатором');
        });
    }

    public createJobInDailyPlan() {
        this.dailyPlanService.createJobInDailyPlan(this.job, this.linkEntityId).subscribe((data: HttpResponse<Job>) => {
            this.loadJobs();
            this.cancelAddJob();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся добавить задачу. Проверьте существуют ли соответсвующие идентификаторы');
        });
    }

    public createJobInEpicTask() {
        this.epicTaskService.createJobInEpicTask(this.job, this.linkEntityId).subscribe((data: HttpResponse<Job>) => {
            this.loadJobs();
            this.cancelAddJob();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся добавить задачу. Проверьте существуют ли соответсвующие идентификаторы');
        });
    }

    public updateJob() {
        this.jobService.updateJob(this.job).subscribe((data: HttpResponse<Job>) => {
            this.loadJobs();
            this.resetCurrentJob();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся обновить задачу. Нет пользователя с заданным идентификатором');
        });
    }

    // ----------------------- check links --------------------- //

    public addJob() {
        switch (this.linkWithEntity) {
            case LinkWithEntity.nothing:
                this.createJob();
                break;
            case LinkWithEntity.dailyPlan:
                this.createJobInDailyPlan();
                break;
            case LinkWithEntity.epicTask:
                this.createJobInEpicTask();
                break;
        }
    }

    public editJob(job: Job) {
        this.job = job;
        this.previousJob = {...job};
    }

    public cancelEditJob() {
        const index = this.jobs.findIndex(elem => elem.id === this.job.id);
        this.jobs[index] = {...this.previousJob};
        this.resetCurrentJob();
    }

    public cancelAddJob() {
        this.resetCurrentJob();
        this.tableMode = true;
    }

    public deleteJob(job: Job) {
        this.jobService.deleteJob(job.id).subscribe(() => this.loadJobs());
    }

    public add() {
        this.resetCurrentJob();
        this.tableMode = false;
        this.linkWithEntity = LinkWithEntity.nothing;
    }

    public onPageChanged(pageNumber: number) {
        this.page = pageNumber;
        this.loadJobs();
    }

    public getPriority(priorityNumber: number): string {
        let result: string;
        switch (priorityNumber) {
            case 1:
                result = 'Низкий';
                break;
            case 2:
                result = 'Средний';
                break;
            case 3:
                result = 'Высокий';
                break;
            default:
                result = 'Не задан';
        }
        return result;
    }

    private resetCurrentJob() {
        this.job = new Job();
    }

    private getJobsSize() {
        this.jobService.getJobs().pipe(
            map((data: Job[]) => data.length)
        ).subscribe((jobCount: number) => {
            this.jobCount = jobCount;
        });
    }
}
