import { Component, OnInit } from '@angular/core';
import { JobToEpicTaskService } from '../../services/job-to-epic-task.service';
import { JobToEpicTask } from '../../models/job-to-epic-task';
import { DatePipe } from '@angular/common';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { parse } from 'querystring';
import { tap, map } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
    templateUrl: 'job-to-epic-task.component.html',
    providers: [JobToEpicTaskService, DatePipe]
})
export class JobToEpicTaskComponent implements OnInit {

    private internalServerErrorStatusCode = 500;

    public pageSize: number;
    public page: number;
    public collectionSize: number;
    public jobToEpicTaskCount: number;
    public jobToEpicTask: JobToEpicTask = new JobToEpicTask();
    public previousJobToEpicTask: JobToEpicTask = new JobToEpicTask();
    public jobToEpicTasks: JobToEpicTask[];
    public tableMode = true;

    constructor(private jobToEpicTaskService: JobToEpicTaskService, private datePipe: DatePipe) {
        this.page = 1;
        this.pageSize = 5;
    }

    ngOnInit() {
        this.loadJobToEpicTasks();
    }

    public loadJobToEpicTasks() {
        this.getJobToEpicTasksSize();
        this.jobToEpicTaskService.getJobToEpicTasks(this.page, this.pageSize).subscribe((data: JobToEpicTask[]) => {
            this.jobToEpicTasks = data;
        });
    }

    public createJobToEpicTask() {
        this.jobToEpicTaskService.createJobToEpicTask(this.jobToEpicTask).subscribe((data: HttpResponse<JobToEpicTask>) => {
            this.loadJobToEpicTasks();
            this.cancelAddJobToEpicTask();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся добавить запись. Проверьте существуют ли данная запись и соответсвующие идентификаторы');
        });
    }

    public updateJobToEpicTask() {
        this.jobToEpicTaskService.updateJobToEpicTask(this.jobToEpicTask).subscribe((data: HttpResponse<JobToEpicTask>) => {
            this.loadJobToEpicTasks();
            this.resetCurrentJobToEpicTask();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся обновить запись. Проверьте существуют ли данная запись и соответсвующие идентификаторы');
        });
    }

    public editJobToEpicTask(jobToEpicTask: JobToEpicTask) {
        this.jobToEpicTask = jobToEpicTask;
        this.previousJobToEpicTask = {...jobToEpicTask};
    }

    public cancelEditJobToEpicTask() {
        const index = this.jobToEpicTasks.findIndex(elem => elem.id === this.jobToEpicTask.id);
        this.jobToEpicTasks[index] = {...this.previousJobToEpicTask};
        this.resetCurrentJobToEpicTask();
    }

    public cancelAddJobToEpicTask() {
        this.resetCurrentJobToEpicTask();
        this.tableMode = true;
    }

    public deleteJobToEpicTask(jobToEpicTask: JobToEpicTask) {
        this.jobToEpicTaskService.deleteJobToEpicTask(jobToEpicTask.id).subscribe(() => this.loadJobToEpicTasks());
    }

    public add() {
        this.resetCurrentJobToEpicTask();
        this.tableMode = false;
    }

    public onPageChanged(pageNumber: number) {
        this.page = pageNumber;
        this.loadJobToEpicTasks();
    }

    private resetCurrentJobToEpicTask() {
        this.jobToEpicTask = new JobToEpicTask();
    }

    private getJobToEpicTasksSize() {
        this.jobToEpicTaskService.getJobToEpicTasks().pipe(
            map((data: JobToEpicTask[]) => data.length)
        ).subscribe((jobToEpicTaskCount: number) => {
            this.jobToEpicTaskCount = jobToEpicTaskCount;
        });
    }
}
