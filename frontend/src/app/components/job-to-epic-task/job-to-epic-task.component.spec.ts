import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobToEpicTaskComponent } from './job-to-epic-task.component';

describe('JobToEpicTaskComponent', () => {
  let component: JobToEpicTaskComponent;
  let fixture: ComponentFixture<JobToEpicTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobToEpicTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobToEpicTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
