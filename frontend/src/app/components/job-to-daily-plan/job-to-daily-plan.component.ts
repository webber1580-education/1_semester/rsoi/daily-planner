import { Component, OnInit } from '@angular/core';
import { JobToDailyPlanService } from '../../services/job-to-daily-plan.service';
import { JobToDailyPlan } from '../../models/job-to-daily-plan';
import { DatePipe } from '@angular/common';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { parse } from 'querystring';
import { tap, map } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
    templateUrl: 'job-to-daily-plan.component.html',
    providers: [JobToDailyPlanService, DatePipe]
})
export class JobToDailyPlanComponent implements OnInit {

    private internalServerErrorStatusCode = 500;

    public pageSize: number;
    public page: number;
    public collectionSize: number;
    public jobToDailyPlanCount: number;
    public jobToDailyPlan: JobToDailyPlan = new JobToDailyPlan();
    public previousJobToDailyPlan: JobToDailyPlan = new JobToDailyPlan();
    public jobToDailyPlans: JobToDailyPlan[];
    public tableMode = true;

    constructor(private jobToDailyPlanService: JobToDailyPlanService, private datePipe: DatePipe) {
        this.page = 1;
        this.pageSize = 5;
    }

    ngOnInit() {
        this.loadJobToDailyPlans();
    }

    public loadJobToDailyPlans() {
        this.getJobToDailyPlansSize();
        this.jobToDailyPlanService.getJobToDailyPlans(this.page, this.pageSize).subscribe((data: JobToDailyPlan[]) => {
            this.jobToDailyPlans = data;
        });
    }

    public createJobToDailyPlan() {
        this.jobToDailyPlanService.createJobToDailyPlan(this.jobToDailyPlan).subscribe((data: HttpResponse<JobToDailyPlan>) => {
            this.loadJobToDailyPlans();
            this.cancelAddJobToDailyPlan();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся добавить запись. Проверьте существуют ли данная запись и соответсвующие идентификаторы');
        });
    }

    public updateJobToDailyPlan() {
        this.jobToDailyPlanService.updateJobToDailyPlan(this.jobToDailyPlan).subscribe((data: HttpResponse<JobToDailyPlan>) => {
            this.loadJobToDailyPlans();
            this.resetCurrentJobToDailyPlan();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся обновить запись. Проверьте существуют ли данная запись и соответсвующие идентификаторы');
        });
    }

    public editJobToDailyPlan(jobToDailyPlan: JobToDailyPlan) {
        this.jobToDailyPlan = jobToDailyPlan;
        this.previousJobToDailyPlan = {...jobToDailyPlan};
    }

    public cancelEditJobToDailyPlan() {
        const index = this.jobToDailyPlans.findIndex(elem => elem.id === this.jobToDailyPlan.id);
        this.jobToDailyPlans[index] = {...this.previousJobToDailyPlan};
        this.resetCurrentJobToDailyPlan();
    }

    public cancelAddJobToDailyPlan() {
        this.resetCurrentJobToDailyPlan();
        this.tableMode = true;
    }

    public deleteJobToDailyPlan(jobToDailyPlan: JobToDailyPlan) {
        this.jobToDailyPlanService.deleteJobToDailyPlan(jobToDailyPlan.id).subscribe(() => this.loadJobToDailyPlans());
    }

    public add() {
        this.resetCurrentJobToDailyPlan();
        this.tableMode = false;
    }

    public onPageChanged(pageNumber: number) {
        this.page = pageNumber;
        this.loadJobToDailyPlans();
    }

    private resetCurrentJobToDailyPlan() {
        this.jobToDailyPlan = new JobToDailyPlan();
    }

    private getJobToDailyPlansSize() {
        this.jobToDailyPlanService.getJobToDailyPlans().pipe(
            map((data: JobToDailyPlan[]) => data.length)
        ).subscribe((jobToDailyPlanCount: number) => {
            this.jobToDailyPlanCount = jobToDailyPlanCount;
        });
    }
}
