import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobToDailyPlanComponent } from './job-to-daily-plan.component';

describe('JobToDailyPlanComponent', () => {
  let component: JobToDailyPlanComponent;
  let fixture: ComponentFixture<JobToDailyPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobToDailyPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobToDailyPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
