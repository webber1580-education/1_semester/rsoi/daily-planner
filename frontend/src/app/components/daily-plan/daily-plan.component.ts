import { Component, OnInit } from '@angular/core';
import { DailyPlanService } from '../../services/daily-plan.service';
import { DailyPlan } from '../../models/daily-plan';
import { DatePipe } from '@angular/common';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { parse } from 'querystring';
import { tap, map } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
    templateUrl: 'daily-plan.component.html',
    styleUrls: ['daily-plan.component.css'],
    providers: [DailyPlanService, DatePipe]
})
export class DailyPlanComponent implements OnInit {

    private internalServerErrorStatusCode = 500;

    public pageSize: number;
    public page: number;
    public collectionSize: number;
    public dailyPlanCount: number;
    public dailyPlan: DailyPlan = new DailyPlan();
    public previousDailyPlan: DailyPlan = new DailyPlan();
    public dailyPlans: DailyPlan[];
    public tableMode = true;

    constructor(private dailyPlanService: DailyPlanService, private datePipe: DatePipe) {
        this.page = 1;
        this.pageSize = 5;
    }

    ngOnInit() {
        this.loadDailyPlans();
    }

    public loadDailyPlans() {
        this.getDailyPlansSize();
        this.dailyPlanService.getDailyPlans(this.page, this.pageSize).subscribe((data: DailyPlan[]) => {
            this.dailyPlans = data;
            for (const dailyPlan of this.dailyPlans) {
                dailyPlan.currentDate = this.datePipe.transform(dailyPlan.currentDate, 'yyyy-MM-dd');
            }
        });
    }

    public createDailyPlan() {
        this.datePickerToDate();
        this.dailyPlanService.createDailyPlan(this.dailyPlan).subscribe((data: HttpResponse<DailyPlan>) => {
            this.loadDailyPlans();
            this.cancelAddDailyPlan();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся добавить дневной план. Нет пользователя с заданным идентификатором');
            this.dateToDatePicker();
        });
    }

    public updateDailyPlan() {
        this.datePickerToDate();
        this.dailyPlanService.updateDailyPlan(this.dailyPlan).subscribe((data: HttpResponse<DailyPlan>) => {
            this.loadDailyPlans();
            this.resetCurrentDailyPlan();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся обновить дневной план. Нет пользователя с заданным идентификатором');
        });
    }

    public editDailyPlan(dailyPlan: DailyPlan) {
        this.dailyPlan = dailyPlan;
        this.previousDailyPlan = {...dailyPlan};
        this.dateToDatePicker();
    }

    public cancelEditDailyPlan() {
        const index = this.dailyPlans.findIndex(elem => elem.id === this.dailyPlan.id);
        this.dailyPlans[index] = {...this.previousDailyPlan};
        this.resetCurrentDailyPlan();
    }

    public cancelAddDailyPlan() {
        this.resetCurrentDailyPlan();
        this.tableMode = true;
    }

    public deleteDailyPlan(dailyPlan: DailyPlan) {
        this.dailyPlanService.deleteDailyPlan(dailyPlan.id).subscribe(() => this.loadDailyPlans());
    }

    public add() {
        this.resetCurrentDailyPlan();
        this.setDatePickerTime();
        this.tableMode = false;
    }

    public onPageChanged(pageNumber: number) {
        this.page = pageNumber;
        this.loadDailyPlans();
    }

    public getDayOfWeek(getDayOfWeek: number): string {
        let result: string;
        switch (getDayOfWeek) {
            case 1:
                result = 'Понедельник';
                break;
            case 2:
                result = 'Вторник';
                break;
            case 3:
                result = 'Среда';
                break;
            case 4:
                result = 'Четверг';
                break;
            case 5:
                result = 'Пятница';
                break;
            case 6:
                result = 'Суббота';
                break;
            case 0:
                result = 'Воскресение';
                break;
            default:
                result = 'Не задан';
        }
        return result;
    }

    private resetCurrentDailyPlan() {
        this.dailyPlan = new DailyPlan();
    }

    private getDailyPlansSize() {
        this.dailyPlanService.getDailyPlans().pipe(
            map((data: DailyPlan[]) => data.length)
        ).subscribe((dailyPlanCount: number) => {
            this.dailyPlanCount = dailyPlanCount;
        });
    }

    private setDatePickerTime() {
        const date = new Date();
        this.dailyPlan.currentDate = {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
        };
    }

    private dateToDatePicker() {
        const date = new Date(this.dailyPlan.currentDate);
        this.dailyPlan.currentDate = {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
        };
    }

    private datePickerToDate() {
        let date = this.dailyPlan.currentDate;
        date = `${date.year}-${date.month}-${date.day}`;
        this.dailyPlan.currentDate = date;
        this.dailyPlan.dayOfWeek = new Date(date).getDay();
    }
}
