import { Component, OnInit } from '@angular/core';
import { EpicTaskService } from '../../services/epic-task.service';
import { EpicTask } from '../../models/epic-task';
import { DatePipe } from '@angular/common';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { parse } from 'querystring';
import { tap, map } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
    templateUrl: 'epic-task.component.html',
    providers: [EpicTaskService, DatePipe]
})
export class EpicTaskComponent implements OnInit {

    private internalServerErrorStatusCode = 500;

    public pageSize: number;
    public page: number;
    public collectionSize: number;
    public epicTaskCount: number;
    public epicTask: EpicTask = new EpicTask();
    public previousEpicTask: EpicTask = new EpicTask();
    public epicTasks: EpicTask[];
    public tableMode = true;

    constructor(private epicTaskService: EpicTaskService, private datePipe: DatePipe) {
        this.page = 1;
        this.pageSize = 5;
    }

    ngOnInit() {
        this.loadEpicTasks();
    }

    public loadEpicTasks() {
        this.getEpicTasksSize();
        this.epicTaskService.getEpicTasks(this.page, this.pageSize).subscribe((data: EpicTask[]) => {
            this.epicTasks = data;
        });
    }

    public createEpicTask() {
        this.epicTaskService.createEpicTask(this.epicTask).subscribe((data: HttpResponse<EpicTask>) => {
            this.loadEpicTasks();
            this.cancelAddEpicTask();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся добавить комплексную задачу. Нет пользователя с заданным идентификатором');
        });
    }

    public updateEpicTask() {
        this.epicTaskService.updateEpicTask(this.epicTask).subscribe((data: HttpResponse<EpicTask>) => {
            this.loadEpicTasks();
            this.resetCurrentEpicTask();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся обновить комплексную задачу. Нет пользователя с заданным идентификатором');
        });
    }

    public editEpicTask(epicTask: EpicTask) {
        this.epicTask = epicTask;
        this.previousEpicTask = {...epicTask};
    }

    public cancelEditEpicTask() {
        const index = this.epicTasks.findIndex(elem => elem.id === this.epicTask.id);
        this.epicTasks[index] = {...this.previousEpicTask};
        this.resetCurrentEpicTask();
    }

    public cancelAddEpicTask() {
        this.resetCurrentEpicTask();
        this.tableMode = true;
    }

    public deleteEpicTask(epicTask: EpicTask) {
        this.epicTaskService.deleteEpicTask(epicTask.id).subscribe(() => this.loadEpicTasks());
    }

    public add() {
        this.resetCurrentEpicTask();
        this.tableMode = false;
    }

    public onPageChanged(pageNumber: number) {
        this.page = pageNumber;
        this.loadEpicTasks();
    }

    private resetCurrentEpicTask() {
        this.epicTask = new EpicTask();
    }

    private getEpicTasksSize() {
        this.epicTaskService.getEpicTasks().pipe(
            map((data: EpicTask[]) => data.length)
        ).subscribe((epicTaskCount: number) => {
            this.epicTaskCount = epicTaskCount;
        });
    }
}
