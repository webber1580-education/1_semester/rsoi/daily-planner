import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpicTaskComponent } from './epic-task.component';

describe('EpicTaskComponent', () => {
  let component: EpicTaskComponent;
  let fixture: ComponentFixture<EpicTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpicTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpicTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
