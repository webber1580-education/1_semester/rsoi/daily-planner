import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { DatePipe } from '@angular/common';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { parse } from 'querystring';
import { tap, map } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
    templateUrl: 'user.component.html',
    providers: [UserService, DatePipe]
})
export class UserComponent implements OnInit {

    private internalServerErrorStatusCode = 500;

    public pageSize: number;
    public page: number;
    public collectionSize: number;
    public userCount: number;
    public user: User = new User();
    public previousUser: User = new User();
    public users: User[];
    public tableMode = true;

    constructor(private userService: UserService, private datePipe: DatePipe) {
        this.page = 1;
        this.pageSize = 5;
    }

    ngOnInit() {
        this.loadUsers();
    }

    public loadUsers() {
        this.getUsersSize();
        this.userService.getUsers(this.page, this.pageSize).subscribe((data: User[]) => {
            this.users = data;
            for (const user of this.users) {
                user.dateOfBirth = this.datePipe.transform(user.dateOfBirth, 'yyyy-MM-dd');
            }
        });
    }

    public createUser() {
        this.userService.createUser(this.user).subscribe((data: HttpResponse<User>) => {
            this.loadUsers();
            this.cancelAddUser();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся добавить пользователя. Произошла внутренняя ошибка');
        });
    }

    public updateUser() {
        this.userService.updateUser(this.user).subscribe((data: HttpResponse<User>) => {
            this.loadUsers();
            this.resetCurrentUser();
        }, (error: HttpErrorResponse) => {
            alert('Не удаётся обновить задачу. Произошла внутренняя ошибка');
        });
    }

    public editUser(user: User) {
        this.user = user;
        this.previousUser = {...user};
    }

    public cancelEditUser() {
        const index = this.users.findIndex(elem => elem.id === this.user.id);
        this.users[index] = {...this.previousUser};
        this.resetCurrentUser();
    }

    public cancelAddUser() {
        this.resetCurrentUser();
        this.tableMode = true;
    }

    public deleteUser(user: User) {
        this.userService.deleteUser(user.id).subscribe(() => this.loadUsers());
    }

    public add() {
        this.resetCurrentUser();
        this.tableMode = false;
    }

    public onPageChanged(pageNumber: number) {
        this.page = pageNumber;
        this.loadUsers();
    }

    private resetCurrentUser() {
        this.user = new User();
    }

    private getUsersSize() {
        this.userService.getUsers().pipe(
            map((data: User[]) => data.length)
        ).subscribe((userCount: number) => {
            this.userCount = userCount;
        });
    }
}
