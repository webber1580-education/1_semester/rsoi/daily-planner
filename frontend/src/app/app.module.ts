import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './components/app/app.component';
import { MenuComponent } from './components/menu/menu.component';
import { UserComponent } from './components/user/user.component';
import { JobComponent } from './components/job/job.component';
import { EpicTaskComponent } from './components/epic-task/epic-task.component';
import { DailyPlanComponent } from './components/daily-plan/daily-plan.component';
import { JobToDailyPlanComponent } from './components/job-to-daily-plan/job-to-daily-plan.component';
import { JobToEpicTaskComponent } from './components/job-to-epic-task/job-to-epic-task.component';

// определение маршрутов
const appRoutes: Routes = [
    { path: '', component: MenuComponent },
    { path: 'users', component: UserComponent },
    { path: 'jobs', component: JobComponent },
    { path: 'epic-tasks', component: EpicTaskComponent },
    { path: 'daily-plans', component: DailyPlanComponent },
    { path: 'job-to-epic-tasks', component: JobToEpicTaskComponent },
    { path: 'job-to-daily-plans', component: JobToDailyPlanComponent },
    { path: '**', redirectTo: '/' }
];

@NgModule({
    imports: [NgbModule, BrowserModule, FormsModule, HttpClientModule, RouterModule.forRoot(appRoutes)],
    declarations: [AppComponent, MenuComponent,
        UserComponent, JobComponent, EpicTaskComponent,
        DailyPlanComponent, JobToDailyPlanComponent, JobToEpicTaskComponent],

    bootstrap: [AppComponent]
})
export class AppModule { }
