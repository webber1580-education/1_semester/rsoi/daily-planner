import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Job } from '../models/job';
import { Observable } from 'rxjs';

@Injectable()
export class JobService {

    private url = `${environment.baseUrl}/api/job`;

    constructor(private http: HttpClient) {}

    public getJobs(page: number = 0, pageSize: number = 0): Observable<Job[]> {
        const from = (page - 1) * pageSize;
        const count = pageSize;
        return this.http.get<Job[]>(`${this.url}?from=${from}&count=${count}`);
    }

    public createJob(job: Job): Observable<HttpResponse<Job>> {
        return this.http.post(this.url, job, { observe: 'response' });
    }

    public updateJob(job: Job): Observable<HttpResponse<Job>> {

        return this.http.put(this.url, job, { observe: 'response' });
    }

    public deleteJob(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }
}
