import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DailyPlan } from '../models/daily-plan';
import { Observable } from 'rxjs';
import { Job } from '../models/job';

@Injectable()
export class DailyPlanService {

    private url = `${environment.baseUrl}/api/dailyPlan`;

    constructor(private http: HttpClient) {}

    public getDailyPlans(page: number = 0, pageSize: number = 0): Observable<DailyPlan[]> {
        const from = (page - 1) * pageSize;
        const count = pageSize;
        return this.http.get<DailyPlan[]>(`${this.url}?from=${from}&count=${count}`);
    }

    public createDailyPlan(dailyPlan: DailyPlan): Observable<HttpResponse<DailyPlan>> {
        return this.http.post(this.url, dailyPlan, { observe: 'response' });
    }

    public updateDailyPlan(dailyPlan: DailyPlan): Observable<HttpResponse<DailyPlan>> {

        return this.http.put(this.url, dailyPlan, { observe: 'response' });
    }

    public deleteDailyPlan(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }

    public createJobInDailyPlan(job: Job, id: number) {
        return this.http.post(`${this.url}/${id}`, job, { observe: 'response' });
    }

}
