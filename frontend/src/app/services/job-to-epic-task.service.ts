import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { JobToEpicTask } from '../models/job-to-epic-task';
import { Observable } from 'rxjs';

@Injectable()
export class JobToEpicTaskService {

    private url = `${environment.baseUrl}/api/jobToEpicTask`;

    constructor(private http: HttpClient) {}

    public getJobToEpicTasks(page: number = 0, pageSize: number = 0): Observable<JobToEpicTask[]> {
        const from = (page - 1) * pageSize;
        const count = pageSize;
        return this.http.get<JobToEpicTask[]>(`${this.url}?from=${from}&count=${count}`);
    }

    public createJobToEpicTask(jobToEpicTask: JobToEpicTask): Observable<HttpResponse<JobToEpicTask>> {
        return this.http.post(this.url, jobToEpicTask, { observe: 'response' });
    }

    public updateJobToEpicTask(jobToEpicTask: JobToEpicTask): Observable<HttpResponse<JobToEpicTask>> {

        return this.http.put(this.url, jobToEpicTask, { observe: 'response' });
    }

    public deleteJobToEpicTask(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }
}
