import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {

    private url = `${environment.baseUrl}/api/user`;

    constructor(private http: HttpClient) {}

    public getUsers(page: number = 0, pageSize: number = 0): Observable<User[]> {
        const from = (page - 1) * pageSize;
        const count = pageSize;
        return this.http.get<User[]>(`${this.url}?from=${from}&count=${count}`);
    }

    public createUser(user: User): Observable<HttpResponse<User>> {
        return this.http.post(this.url, user, { observe: 'response' });
    }

    public updateUser(user: User): Observable<HttpResponse<User>> {

        return this.http.put(this.url, user, { observe: 'response' });
    }

    public deleteUser(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }
}
