import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { EpicTask } from '../models/epic-task';
import { Observable } from 'rxjs';
import { Job } from '../models/job';

@Injectable()
export class EpicTaskService {

    private url = `${environment.baseUrl}/api/epicTask`;

    constructor(private http: HttpClient) {}

    public getEpicTasks(page: number = 0, pageSize: number = 0): Observable<EpicTask[]> {
        const from = (page - 1) * pageSize;
        const count = pageSize;
        return this.http.get<EpicTask[]>(`${this.url}?from=${from}&count=${count}`);
    }

    public createEpicTask(epicTask: EpicTask): Observable<HttpResponse<EpicTask>> {
        return this.http.post(this.url, epicTask, { observe: 'response' });
    }

    public updateEpicTask(epicTask: EpicTask): Observable<HttpResponse<EpicTask>> {

        return this.http.put(this.url, epicTask, { observe: 'response' });
    }

    public deleteEpicTask(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }

    public createJobInEpicTask(job: Job, id: number) {
        return this.http.post(`${this.url}/${id}`, job, { observe: 'response' });
    }
}
