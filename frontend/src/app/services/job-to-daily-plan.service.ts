import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { JobToDailyPlan } from '../models/job-to-daily-plan';
import { Observable } from 'rxjs';

@Injectable()
export class JobToDailyPlanService {

    private url = `${environment.baseUrl}/api/jobToDailyPlan`;

    constructor(private http: HttpClient) {}

    public getJobToDailyPlans(page: number = 0, pageSize: number = 0): Observable<JobToDailyPlan[]> {
        const from = (page - 1) * pageSize;
        const count = pageSize;
        return this.http.get<JobToDailyPlan[]>(`${this.url}?from=${from}&count=${count}`);
    }

    public createJobToDailyPlan(jobToDailyPlan: JobToDailyPlan): Observable<HttpResponse<JobToDailyPlan>> {
        return this.http.post(this.url, jobToDailyPlan, { observe: 'response' });
    }

    public updateJobToDailyPlan(jobToDailyPlan: JobToDailyPlan): Observable<HttpResponse<JobToDailyPlan>> {

        return this.http.put(this.url, jobToDailyPlan, { observe: 'response' });
    }

    public deleteJobToDailyPlan(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }
}
